//===========================================================
// Nombre      : pruebas_GastroEDI.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Pruebas unitarias de los modulos de
//===========================================================

#include "../src/GastroEDI.h"

class pruebas_GastroEDI{

	GastroEDI *GastroEDITest;

public:

	pruebas_GastroEDI();

	~pruebas_GastroEDI();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Se hace una consulta de un visitante NULO
	 *
	 * Condiciones finales:
	 *
	 * Debería mostrar mensaje de error
	 *
	 */

	bool prueba_Consulta();


	bool prueba_Distribución();


};

pruebas_GastroEDI::pruebas_GastroEDI(){

prueba_Consulta();

this->prueba_Distribución();


}

pruebas_GastroEDI::~pruebas_GastroEDI(){



}

bool pruebas_GastroEDI::prueba_Consulta(){

GastroEDITest = new GastroEDI();

cout << "Inserte Visitante NULO";

GastroEDITest->consultarRestaurante();


return true;

}

bool pruebas_GastroEDI::prueba_Distribución(){

GastroEDITest = new GastroEDI();

GastroEDITest->distribuirVisitantes();

delete GastroEDITest;


return true;

}
