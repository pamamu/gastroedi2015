//===========================================================
// Nombre      : pruebas_PreferenciasCOLA.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Pruebas unitarias de los modulos de
//===========================================================

#include "../src/Preferencias.h"

class pruebas_Preferencias {

private:

	Preferencias *PrefereneciasTest;

public:

	pruebas_Preferencias();

	~pruebas_Preferencias();

	/* Prueba Mostrar
	 * Consiste en probar el método Mostrar.
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *	Cola vacia
	 *
	 * Condiciones finales:
	 *
	 *	No se mostrará nada
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *	Cola con dos valores (El bar de Pepito, El bar de Menganito)
	 *
	 * Condiciones finales:
	 *
	 *
	 *
	 */

	bool pruebas_mostrar();

	/* Prueba insertarPreferencia
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *	La cola esta vacía e insertamos un valor nulo
	 *
	 * Condiciones finales:
	 *
	 *	Se muestra la cola, en la que deberá haber un valor nulo
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * La cola esta vacía y se insertan tres valores (AAA, BBB, CCC)
	 *
	 * Condiciones finales:
	 *
	 * Se muestra la cola, en la cual deberán estar los valores en orden AAA - BBB - CCC
	 *
	 */

	bool pruebas_insertarPreferencia();

	/* Prueba borrarPreferencia
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *	La cola esta vacía y la preferencia a borrar es "PruebaTest"
	 *
	 * Condiciones finales:
	 *
	 *	Devuelve FALSE y la cola no se modifica
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * En la cola hay valores y la preferencia a borrar es ""
	 *
	 * Condiciones finales:
	 *
	 * Devuelve FALSE y la cola no se modifica
	 *
	 * --------------------------
	 * 3º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * En la cola hay valores y la preferencia a borrar es "AAA" (está en la cola)
	 *
	 * Condiciones finales:
	 *
	 * Devuelve TRUE y se elimina el valor
	 *
	 */

	bool pruebas_borrarPreferencia();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *	La cola esta vacía y la preferencia a buscar es "PruebaTest"
	 *
	 * Condiciones finales:
	 *
	 *	Devuelve FALSE
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * En la cola hay valores y la preferencia a buscar es ""
	 *
	 * Condiciones finales:
	 *
	 * Devuelve FALSE
	 *
	 * --------------------------
	 * 3º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * En la cola hay valores y la preferencia a buscar es "AAA" (está en la cola)
	 *
	 * Condiciones finales:
	 *
	 * Devuelve TRUE
	 *
	 */

	bool pruebas_buscarPreferencia();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * La cola esta vacía
	 *
	 * Condiciones finales:
	 *
	 * Devuelve ""
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *
	 * La cola esta vacía y se insertan tres valores (AAA, BBB, CCC)
	 *
	 *
	 * Condiciones finales:
	 *
	 *
	 * El primer elemento será AAA y la cola tendrá un elemento menos (AAA)
	 *
	 */

	bool pruebas_primeraPreferencia();

};

//Implementacion

pruebas_Preferencias::pruebas_Preferencias() {

	if (!pruebas_mostrar())

		cout << "ERROR: mostrar()" << endl;

	if (!pruebas_insertarPreferencia())

		cout << "ERROR: insertarPreferencia()" << endl;

	if (!pruebas_borrarPreferencia())

		cout << "ERROR: borrarPreferencia()" << endl;

	if (!pruebas_buscarPreferencia())

		cout << "ERROR: buscarPreferencia()" << endl;

	if (!pruebas_primeraPreferencia())

		cout << "ERROR: primeraPreferencia()" << endl;

}

pruebas_Preferencias::~pruebas_Preferencias() {

	delete this->PrefereneciasTest;

}

bool pruebas_Preferencias::pruebas_mostrar() {

	cout << "\npruebas_Preferencias::pruebas_mostrar" << endl;

	//1º Caso

	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//2º Caso

	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("El bar de Pepito");

	this->PrefereneciasTest->insertarPreferencia("El bar de Menganito");

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	return true;
}

bool pruebas_Preferencias::pruebas_insertarPreferencia() {

	cout << "\npruebas_Preferencias::pruebas_insertarPreferencia" << endl;

	//1º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("");

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//2º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	return true;
}

bool pruebas_Preferencias::pruebas_borrarPreferencia() {

	cout << "\npruebas_Preferencias::pruebas_borrarPreferencia" << endl;

	//1º Caso
	this->PrefereneciasTest = new Preferencias;

	cout << "Se eliminará PruebaTest";

	if (PrefereneciasTest->borrarPreferencia("PruebaTest"))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//2º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	cout << "Se eliminará \"\"";

	if (PrefereneciasTest->borrarPreferencia(""))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//3º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	this->PrefereneciasTest->mostrar();

	cout << "Se eliminará \"AAA\"";

	if (!PrefereneciasTest->borrarPreferencia("AAA"))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	return true;
}

bool pruebas_Preferencias::pruebas_buscarPreferencia() {

	cout << "\npruebas_Preferencias::pruebas_buscarPreferencia" << endl;

	//1º Caso
	this->PrefereneciasTest = new Preferencias;

	if (PrefereneciasTest->buscarPreferencia("PruebaTest"))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//2º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	if (PrefereneciasTest->buscarPreferencia(""))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//3º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	this->PrefereneciasTest->mostrar();

	if (!PrefereneciasTest->buscarPreferencia("AAA"))

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	return true;

}

bool pruebas_Preferencias::pruebas_primeraPreferencia() {

	cout << "\npruebas_Preferencias::pruebas_primeraPreferencia" << endl;

	//1º Caso
	this->PrefereneciasTest = new Preferencias;

	if (PrefereneciasTest->primeraPreferencia() != "")

		return false;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;

	//2º Caso
	this->PrefereneciasTest = new Preferencias;

	this->PrefereneciasTest->insertarPreferencia("AAA");

	this->PrefereneciasTest->insertarPreferencia("BBB");

	this->PrefereneciasTest->insertarPreferencia("CCC");

	this->PrefereneciasTest->mostrar();

	if (this->PrefereneciasTest->primeraPreferencia()!= "AAA")

		return false;

	cout << "\nDespues de sacar la primera Preferencia: " << endl;

	this->PrefereneciasTest->mostrar();

	delete PrefereneciasTest;



	return true;
}
