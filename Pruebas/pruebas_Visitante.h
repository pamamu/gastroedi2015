//===========================================================
// Nombre      : pruebas_Visitante.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Pruebas unitarias de los modulos de
//===========================================================

#include "../src/Visitante.h"

class pruebas_Visitante {

	Visitante *VisitanteTest;

public:

	pruebas_Visitante();

	~pruebas_Visitante();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * El visitante se ha creado por defecto y se le inserta una preferencia NULA
	 *
	 * Condiciones finales:
	 *
	 * EL visitante tendrá un preferencia NULA
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * EL visitante se ha creado por defecto y se le insertan preferencias con el orden AAA - BBB - CCC
	 *
	 * Condiciones finales:
	 *
	 * El visitante contendrá las preferencias en orden de insercción
	 *
	 */

	bool pruebas_insertarPreferencia();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * El visitante se ha creado por defecto y no tiene preferecencias almacenadas
	 *
	 * Condiciones finales:
	 *
	 * La primera preferencia será "" y devuelve FALSE
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * EL visitante se ha creado por defecto y tiene almacenadas las preferencias en orden AAA - BBB - CCC
	 *
	 * Condiciones finales:
	 *
	 * Las segunda preferencia tiene que ser BBB y devolver TRUE
	 *
	 */

	bool pruebas_getPreferencia();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * El visitante se ha creado por defecto y no tiene preferencias almacenadas
	 *
	 * Borramos la preferencia "AAA"
	 *
	 * Condiciones finales:
	 *
	 * Devuelve FALSE
	 *
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * EL visitante se ha creado por defecto y tiene almacenadas las preferencias en orden AAA - BBB - CCC
	 *
	 * Borramos la preferencia "AAA"
	 *
	 * Condiciones finales:
	 *
	 * Devuelve TRUE y la preferencia es borrada
	 *
	 */

	bool pruebas_borrarPreferencia();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Visitante creado por defecto
	 *
	 * Condiciones finales:
	 *
	 * EL voto tiene que ser mayor que 0 y menor que la máxima valoración
	 *
	 */

	bool pruebas_votar();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Se crea un visitante por defecto y otro parametrizado con Nombre = Andrea y Apellidos = Sánchez Linares
	 *
	 *
	 * Condiciones finales:
	 *
	 * Devuelve FALSE
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Se crean dos visitantes por defecto
	 *
	 * Condiciones finales:
	 *
	 * Devuelve TRUE
	 *
	 */

	bool pruebas_operatorigual();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *
	 * Condiciones finales:
	 *
	 *
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 *
	 * Condiciones finales:
	 *
	 *
	 *
	 */

	bool pruebas_mostrar();

};

//Implementacion

pruebas_Visitante::pruebas_Visitante() {

	if (!pruebas_insertarPreferencia())

		cout << "ERROR: insertarPreferencia()" << endl;

	;

	if (!pruebas_getPreferencia())

		cout << "ERROR: getPreferencia()" << endl;

	;

	if (!pruebas_borrarPreferencia())

		cout << "ERROR: borrarPreferencia()" << endl;

	;

	if (!pruebas_votar())

		cout << "ERROR: votar()" << endl;

	;

	if (!pruebas_operatorigual())

		cout << "ERROR: operatorigual()" << endl;

	;

	if (!pruebas_mostrar())

		cout << "ERROR: mostrar()" << endl;

	;

}

pruebas_Visitante::~pruebas_Visitante() {

}

bool pruebas_Visitante::pruebas_insertarPreferencia() {

	cout << "\npruebas_Visitante::pruebas_insertarPreferencia" << endl;

	//1º Caso

	this->VisitanteTest = new Visitante;

	VisitanteTest->insertarPreferencia("");

	VisitanteTest->mostrar();

	delete VisitanteTest;

	//2º Caso

	this->VisitanteTest = new Visitante;

	this->VisitanteTest->insertarPreferencia("AAA");

	this->VisitanteTest->insertarPreferencia("BBB");

	this->VisitanteTest->insertarPreferencia("CCC");

	VisitanteTest->mostrar();

	delete VisitanteTest;

	return true;
}

bool pruebas_Visitante::pruebas_getPreferencia() {

	cout << "\npruebas_Visitante::pruebas_getPreferencia" << endl;

	string Paux;

	//1º Caso

	this->VisitanteTest = new Visitante;

	if (this->VisitanteTest->getPreferencia(Paux))

		if (Paux != "")

			return false;

	delete VisitanteTest;

	//2º Caso

	this->VisitanteTest = new Visitante;

	this->VisitanteTest->insertarPreferencia("AAA");

	this->VisitanteTest->insertarPreferencia("BBB");

	this->VisitanteTest->insertarPreferencia("CCC");

	this->VisitanteTest->getPreferencia(Paux);

	if (!this->VisitanteTest->getPreferencia(Paux))

		return false;

	if (Paux != "BBB")

		return false;

	delete VisitanteTest;

	return true;
}

bool pruebas_Visitante::pruebas_borrarPreferencia() {

	//1º Caso

	this->VisitanteTest = new Visitante;

	if (this->VisitanteTest->borrarPreferencia("AAA"))

		return false;

	delete VisitanteTest;

	//2º Caso

	this->VisitanteTest = new Visitante;

	this->VisitanteTest->insertarPreferencia("AAA");

	this->VisitanteTest->insertarPreferencia("BBB");

	this->VisitanteTest->insertarPreferencia("CCC");

	if (!this->VisitanteTest->borrarPreferencia("AAA"))

		return false;

	VisitanteTest->mostrar();

	delete VisitanteTest;

	return true;
}

bool pruebas_Visitante::pruebas_votar() {

	//1º Caso

	this->VisitanteTest = new Visitante;

	if (VisitanteTest->votar() < 0 || VisitanteTest->votar() > MAXVALORACION)

		return false;

	delete VisitanteTest;

	return true;
}

bool pruebas_Visitante::pruebas_operatorigual() {

	//1º Caso

	this->VisitanteTest = new Visitante;

	Visitante *Vaux2 = new Visitante("Andrea", "Sánchez Linares");

	if (*VisitanteTest == *Vaux2)

		return false;

	delete Vaux2;

	delete VisitanteTest;

	//2º Caso

	this->VisitanteTest = new Visitante;

	Vaux2 = new Visitante();

	if (!(*VisitanteTest == *Vaux2))

		return false;

	delete Vaux2;

	delete VisitanteTest;

	return true;
}

bool pruebas_Visitante::pruebas_mostrar() {

	//1º Caso

	this->VisitanteTest = new Visitante;

	this->VisitanteTest->mostrar();

	delete VisitanteTest;

	//2º Caso

	this->VisitanteTest = new Visitante("Andrea", "Sánchez Linares", "Badajoz",
			2);

	this->VisitanteTest->mostrar();

	delete VisitanteTest;

	return true;
}
