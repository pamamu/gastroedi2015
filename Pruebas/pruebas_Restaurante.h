//===========================================================
// Nombre      : pruebas_Restaurante.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Pruebas unitarias de los modulos de
//===========================================================

#include "../src/Restaurante.h"

class pruebas_Restaurante {

	Restaurante* RestauranteTest;

public:

	pruebas_Restaurante();

	~pruebas_Restaurante();

	/* Prueba ()
	 * --------------------------
	 * 1º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Visitante por defecto con 50 comensales y aforo restaurante es 30
	 *
	 *
	 * Condiciones finales:
	 *
	 *	Visitante no se inserta y devuelve FALSE
	 *
	 * --------------------------
	 * 2º Caso
	 * --------------------------
	 * Condiciones iniciales:
	 *
	 * Visitante por defecto con 30 comensales y aforo restaurante es 30
	 *
	 * Condiciones finales:
	 *
	 * Visitante se inserta y devuelve true
	 * 0 sitios libres
	 *
	 */

bool pruebas_insertarReserva();

/* Prueba ()
 * --------------------------
 * 1º Caso
 * --------------------------
 * Condiciones iniciales:
 *
 * El Nombre y el apellido del Visitante no corresponde a ningún Visitante
 *
 *
 * Condiciones finales:
 *
 * Devuelve flase y no modofica nada
 *
 * --------------------------
 * 2º Caso
 * --------------------------
 * Condiciones iniciales:
 *
 * El nombre y el apellido del visitante corresponde a un Visitante que contiene 10 comensales
 *
 * Condiciones finales:
 *
 * Se elimina del restaurante y disminuye la ocupación en 10
 * Devuelve true
 *
 */

bool pruebas_deleteReserva();

};

pruebas_Restaurante::pruebas_Restaurante(){

	if (!pruebas_insertarReserva())

			cout << "ERROR: insertarReserva()" << endl;

	;

	if (!pruebas_deleteReserva())

			cout << "ERROR: deleteReserva()" << endl;

	;


}

pruebas_Restaurante::~pruebas_Restaurante(){

}



bool pruebas_Restaurante::pruebas_insertarReserva() {

	this->RestauranteTest = new Restaurante();

	Visitante *Vaux = new Visitante();

	//1º Caso

	RestauranteTest->setAforo(30);

	Vaux->setComensales(50);

	if(RestauranteTest->insertarReserva(Vaux))

		return false;

	RestauranteTest->mostrar();

	//2º Caso

	RestauranteTest->setAforo(30);

	Vaux->setComensales(30);

	if(!RestauranteTest->insertarReserva(Vaux))

		return false;

	if(RestauranteTest->getSitiosLibres()!=0 && RestauranteTest->getOcupacion()!=30)

		return false;

	RestauranteTest->mostrar();

	return true;

}

bool pruebas_Restaurante::pruebas_deleteReserva() {

	this->RestauranteTest = new Restaurante();

	Visitante *Vaux = new Visitante();

	//1º Caso

	if(RestauranteTest->deleteReserva("Hortensio", "Pajares"))

		return false;

	RestauranteTest->mostrar();

	//2º Caso

	RestauranteTest->setAforo(15);

	Vaux->setComensales(10);

	Vaux->setNombre("Amelio");

	RestauranteTest->insertarReserva(Vaux);

	if(!RestauranteTest->deleteReserva("Amelio", ""))

		return false;

	return true;

}


