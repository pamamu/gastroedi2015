//=================================================================================================
// Nombre      : Restaurantes.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementación de la clase Restaurantes
//=================================================================================================

#include "Restaurantes.h"

//Métodos Privados

void Restaurantes::mostrarArbolOrden( Arbol<Restaurante *, CmpRestaurante> *arbol) {

	if (!arbol->vacio()) {

		if (arbol->hijoIzq() != NULL)

			mostrarArbolOrden(arbol->hijoIzq());

		arbol->raiz()->mostrar();

		if (arbol->hijoDer() != NULL)

		mostrarArbolOrden(arbol->hijoDer());

	}

}

void Restaurantes::salidaArbol(Arbol<Restaurante *, CmpRestaurante> *arbol, ofstream &flujo_salida){

	if (!arbol->vacio()) {

		if (arbol->hijoIzq() != NULL)

			salidaArbol(arbol->hijoIzq(), flujo_salida);

		arbol->raiz()->salida(flujo_salida);

		if (arbol->hijoDer() != NULL)

		salidaArbol(arbol->hijoDer(), flujo_salida);

	}


}

void Restaurantes::votarArbol(Arbol<Restaurante *, CmpRestaurante> *arbol) {

	if (!arbol->vacio()) {

		if (arbol->hijoIzq() != NULL)

			votarArbol(arbol->hijoIzq());

		arbol->raiz()->votacion();

		if (arbol->hijoDer() != NULL)

			votarArbol(arbol->hijoDer());

	}


}

bool Restaurantes::buscarenArbol_Val(Arbol<Restaurante *, CmpRestaurante> *arbol, Restaurante* &Raux, float valmax, const string& smax) {

	if (!arbol->vacio()) {

		if (arbol->hijoIzq() != NULL)

			buscarenArbol_Val(arbol->hijoIzq(), Raux, valmax, smax);

		//

		if (arbol->raiz()->getValoracion() < valmax
				&& arbol->raiz()->getValoracion() > Raux->getValoracion()) {

			Raux = arbol->raiz();

		}

		else {

			if (arbol->raiz()->getValoracion() == valmax) {

				if (arbol->raiz()->getNombre().compare(Raux->getNombre()) > 0
						&& arbol->raiz()->getNombre().compare(smax) < 0)

					Raux = arbol->raiz();
			}

			else if (arbol->raiz()->getValoracion() == Raux->getValoracion()
					&& arbol->raiz()->getNombre().compare(Raux->getNombre())
							> 0)

				Raux = arbol->raiz();

		}
		//

		if (arbol->hijoDer() != NULL)

			buscarenArbol_Val(arbol->hijoDer(), Raux, valmax, smax);

		return true;

	}

	return false;

}

bool Restaurantes::buscarenArbol_Raiz(Arbol<Restaurante *, CmpRestaurante> *arbol, const string &raiz){

	if (!arbol->vacio()) { //Si el arbol no está vacio

		if (arbol->raiz()->getNombre().compare(0, raiz.size(), raiz) > 0) { //Si el nombre del restaurante de la raiz es mayor que el de la raiz

			if (arbol->hijoIzq() == NULL)

				return false;

			return buscarenArbol_Raiz(arbol->hijoIzq(), raiz); //Devuelve el valor de la llamada a buscarenArbol_Raiz con el hijo izq

		}

		else if (arbol->raiz()->getNombre().compare(0, raiz.size(), raiz) < 0) { //Si el nombre del restaurante de la raiz es menor que el de la raiz

			if (arbol->hijoDer() == NULL)

				return false;

			return buscarenArbol_Raiz(arbol->hijoDer(), raiz); //Devuelve el valor de la llamada a buscarenArbol_Raiz con el hijo der

		}

			else { //El valor del restaurante de la raiz es el mismo que el de la raiz

			arbol->raiz()->mostrar(); //Muestra el restaurante

			if (arbol->hijoDer() != NULL)

				buscarenArbol_Raiz(arbol->hijoDer(), raiz);

			if (arbol->hijoIzq() != NULL)

				buscarenArbol_Raiz(arbol->hijoIzq(), raiz);

			return true; //Devuelve true

		}

	}

	return false;  //Devuelve false

}

bool Restaurantes::buscarenArbol_Nom(
		Arbol<Restaurante *, CmpRestaurante> *arbol, Restaurante* &Raux,
		const string &nombre) {

	if (!arbol->vacio()) {

		if (nombre.compare(arbol->raiz()->getNombre()) < 0) { //Si el nombre es menor que el restaurante de la raiz, y el hijo izq no es NULL

			if (arbol->hijoIzq() == NULL)

				return false;

			return (buscarenArbol_Nom(arbol->hijoIzq(), Raux, nombre));

		}

		else if (nombre.compare(arbol->raiz()->getNombre()) > 0) { //Si el nombre es mayor que el restaurante de la raiz, y el hijo der no es NULL

			if (arbol->hijoDer() == NULL)

				return false;

			return (buscarenArbol_Nom(arbol->hijoDer(), Raux, nombre));

		}

		else { //ENCONTRADO

			Raux = arbol->raiz();

			return true;

		}

	}

	Raux = NULL;

	return false;

}

//Constructores y destructores

Restaurantes::Restaurantes() {

	restaurantes = new Arbol<Restaurante *, CmpRestaurante>();

}

Restaurantes::~Restaurantes() {

	delete restaurantes;

	restaurantes = NULL;

}

void Restaurantes::mostrar() {

	mostrarArbolOrden(restaurantes);

}

void Restaurantes::insertarRestaurante(Restaurante* Res) {

	restaurantes->insertar(Res);

}

void Restaurantes::borrarRestaurante(const string &nombre) {

	Restaurante *Raux = new Restaurante();

	Raux->setNombre(nombre);

	restaurantes->borrar(Raux);

	delete Raux;

	Raux = NULL;

}

bool Restaurantes::buscarRestaurante(const string &nombre, Restaurante* &R) {

	return buscarenArbol_Nom(restaurantes, R, nombre);

}

void Restaurantes::buscarCoincidencia(const string &raiz) {

if(!buscarenArbol_Raiz(restaurantes, raiz))

	cout << "No se ha encontrado ningún restaurante con esa raiz" << endl;

}

void Restaurantes::votacion() {

	votarArbol(restaurantes);

}

bool Restaurantes::masValorado(Restaurante* &R, float valmax, const string &smax) {

	R = new Restaurante();

	R->setValoracion(0);

	R->setNombre("");

	if(!buscarenArbol_Val(restaurantes, R, valmax, smax))

			return false;

	else if(R->getNombre()==""){

		delete R;

		R = NULL;

		return false;

	}

	return true;

}

void Restaurantes::salida(ofstream &flujo_escritura){

	this->salidaArbol(restaurantes, flujo_escritura);

}
