//=================================================================================================
// Nombre      : Visitante.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementanción de la clase Visitante
//=================================================================================================

#include <iostream>

#include "Visitante.h"

//Constructores y destructores

Visitante::Visitante() {

	Nombre = "";

	Apellidos = "";

	Ciudad = "";

	Comensales = 0;

	Atendido = false;

	Eleccion = 0;

	BD_Preferencias = new Preferencias();

	cout << "Clase Visitante - Ejecutado constructor por defecto" << endl;

}

Visitante::Visitante(string _Nombre, string _Apellidos) {

	Nombre = _Nombre;

	Apellidos = _Apellidos;

	Ciudad = "";

	Comensales = 0;

	Atendido = false;

	Eleccion = 0;

	BD_Preferencias = new Preferencias();

	cout << "Clase Visitante - Ejecutado constructor parametrizado" << endl;

}

Visitante::Visitante(string _Nombre, string _Apellidos, string _Ciudad,
		int _Comensales) {

	Nombre = _Nombre;

	Apellidos = _Apellidos;

	Ciudad = _Ciudad;

	Comensales = _Comensales;

	Atendido = false;

	Eleccion = 0;

	BD_Preferencias = new Preferencias();

	cout << "Clase Visitante - Ejecutado constructor parametrizado" << endl;

}

Visitante::~Visitante() {

	delete BD_Preferencias;

	BD_Preferencias = NULL;

	cout << "Clase Visitante - Ejecutado destructor" << endl;

}

//Métodos Públicos

void Visitante::setNombre(string _Nombre) {

	Nombre = _Nombre;

}

void Visitante::setApellidos(string _Apellidos) {

	Apellidos = _Apellidos;

}

void Visitante::setCiudad(string Ciudad) {

	this->Ciudad = Ciudad;

}

void Visitante::setComensales(int _Comensales) {

	Comensales = _Comensales;

}

void Visitante::setAtendido(bool _Atendido) {

	Atendido = _Atendido;

}

void Visitante::setEleccion(int _Eleccion) {

	Eleccion = _Eleccion;

}

string Visitante::getNombre() const {

	return Nombre;

}

string Visitante::getApellidos() const {

	return Apellidos;

}

string Visitante::getCiudad() const {

	return Ciudad;

}

bool Visitante::getAtendido() const {

	return Atendido;

}

int Visitante::getEleccion() const {

	return Eleccion;

}

int Visitante::getComensales() const {

	return Comensales;

}

void Visitante::insertarPreferencia(string _Restaurante) {

	BD_Preferencias->insertarPreferencia(_Restaurante);

}

bool Visitante::getPreferencia(string &_Restaurante) const {

	_Restaurante = BD_Preferencias->primeraPreferencia();

	return _Restaurante != "";

}

bool Visitante::borrarPreferencia(const string &_Restaurante) {

	return BD_Preferencias->borrarPreferencia(_Restaurante);

}

void Visitante::recuperarPreferencias(Preferencias *PreAux) {

	string Paux = PreAux->primeraPreferencia();

	while (Paux != "") {

		this->insertarPreferencia(Paux);

		Paux = PreAux->primeraPreferencia();

	}

}

void Visitante::borrarPreferencias() {

	delete BD_Preferencias;

	BD_Preferencias = new Preferencias;

}

int Visitante::votar() {

	int numero = GenAleatorios::generarNumero(MAXVALORACION);

	return numero;

}

bool Visitante::operator ==(const Visitante& per) {

	return ((Nombre == per.Nombre) && (Apellidos == per.Apellidos));

}

void Visitante::mostrar() {

	cout << "============================================"
			"\n-- Nombre = " << Nombre
			<< "\n--------------------------------------------"
					"\n-- Apellidos = " << Apellidos
			<< "\n--------------------------------------------"
					"\n-- Ciudad  =  " << Ciudad
			<< "\n--------------------------------------------"
					"\n-- Comensales = " << Comensales
			<< "\n--------------------------------------------"
					"\n-- Atendido = " << ((Atendido) ? "SI" : "NO")
			<< "\n--------------------------------------------" << endl;
	BD_Preferencias->mostrar();

}

void Visitante::salida(ofstream &flujo_salida) {

	flujo_salida << Nombre << "#" << Apellidos << "#" << Ciudad << "#"
			<< Comensales << "#" << Eleccion << endl;

}

void Visitante::salidaNA(ofstream &flujo_salida) {

	flujo_salida << Nombre << " " << Apellidos << "#" << Ciudad << "#"
			<< Comensales << endl;

}
