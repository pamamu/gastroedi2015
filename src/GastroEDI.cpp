//=================================================================================================
// Nombre      : GastroEDI.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Implementanci�n de la clase GastroEDI
//=================================================================================================

#include "GastroEDI.h"

void GastroEDI::Salida_Valoraciones(){

	Restaurante *Raux;

	float valmax = MAXVALORACION;

	string smax = "{";

	ofstream flujo_escritura;

	flujo_escritura.open("Logs/restaurantesmasvalorados.log", ios::trunc);

	while(BD_Restaurantes->masValorado(Raux, valmax, smax)){

		flujo_escritura << Raux->getNombre() << "#" << Raux->getOcupacion() << "#" << Raux->getValoracion() << endl;

		valmax = Raux->getValoracion();

		smax = Raux->getNombre();

	}

	flujo_escritura.close();


}

void GastroEDI::Salida_Registro(){

	//Algoritmo para calcular dia y mes actual del sistema

	time_t sisTime;

	struct tm *tiempo;

	time(&sisTime);

	tiempo=localtime(&sisTime);

	int dia = tiempo->tm_mday;

	int mes = tiempo->tm_mon+1;

	//Fin Algoritmo

	ofstream flujo_escritura;

	stringstream nombre_archivo;

	nombre_archivo << "Logs/gastroedi" << dia << "_" << mes << ".log";

	flujo_escritura.open(nombre_archivo.str().c_str() , ios::trunc);

	BD_Restaurantes->salida(flujo_escritura);

	flujo_escritura.close();


}

void GastroEDI::Salida_NoAtendidos(){

	ofstream flujo_escritura;

	flujo_escritura.open("Logs/visitantesnoatendidos.log" , ios::trunc);

	BD_VisitantesNA->salida(flujo_escritura);

	flujo_escritura.close();

}

GastroEDI::GastroEDI() {

	BD_Restaurantes = new Restaurantes();

	BD_Visitantes = new Visitantes();

	BD_VisitantesNA = new VisitantesNA();

}

GastroEDI::~GastroEDI() {

	delete BD_Restaurantes;

	BD_Restaurantes = NULL;

	delete BD_Visitantes;

	BD_Visitantes = NULL;

	delete BD_VisitantesNA;

	BD_VisitantesNA = NULL;

}

void GastroEDI::cargarRestaurantes() {

	ifstream flujo_lectura;

	string Nombre, Direccion, Telefono, SNumTenedores, SAforo;

	int NumTenedores, Aforo;

	Restaurante *r;

	flujo_lectura.open("Datos/restaurantes.txt");

	if (!flujo_lectura.fail()) {

		while (!flujo_lectura.eof()) {

			getline(flujo_lectura, Nombre, '#');

			getline(flujo_lectura, Direccion, '#');

			getline(flujo_lectura, SNumTenedores, '#');

			getline(flujo_lectura, Telefono, '#');

			getline(flujo_lectura, SAforo, '\n');

			NumTenedores = atoi(SNumTenedores.c_str());

			Aforo = atoi(SAforo.c_str());

			r = new Restaurante(Nombre, Direccion, NumTenedores, Aforo,
					Telefono);

			BD_Restaurantes->insertarRestaurante(r);

		}

		cout << "\n================================="
				"\n==       BD_Restaurantes          =="
				"\n==         CARGADOS            =="
				"\n=================================" << endl;

	}

	else

		cout << "ERROR : Error en lectura del archivo de carga de datos"
				<< endl;



	flujo_lectura.close();

	r = NULL;

	cin.get();

}

void GastroEDI::cargarVisitantes() {

	string Nombre, Apellidos, Ciudad, SComensales, SPreferencias, Preferencia;

	int Comensales, Preferencias;

	Visitante *v;

	ifstream flujo_lectura;

	flujo_lectura.open("Datos/visitantes.txt");

	if (!flujo_lectura.fail()) {

		while (!flujo_lectura.eof()) {

			getline(flujo_lectura, Nombre, '#');

			getline(flujo_lectura, Apellidos, '#');

			getline(flujo_lectura, Ciudad, '#');

			getline(flujo_lectura, SComensales, '#');

			getline(flujo_lectura, SPreferencias, '\n');

			Comensales = atoi(SComensales.c_str());

			Preferencias = atoi(SPreferencias.c_str());

			v = new Visitante(Nombre, Apellidos, Ciudad, Comensales);

			for (int i = 0; i < Preferencias; i++) {

				getline(flujo_lectura, Preferencia, '\n');

				v->insertarPreferencia(Preferencia);

			}

			BD_Visitantes->insertarVisitante(v);

		}

		cout << "\n================================="
				"\n==        BD_Visitantes           =="
				"\n==         CARGADOS            =="
				"\n=================================" << endl;

	}

	else

		cout << "ERROR : Error en lectura del archivo de carga de datos"
				<< endl;

	flujo_lectura.close();

	v = NULL;

	cin.get();

}

void GastroEDI::consultarVisitante() {

	string Nombre_Visitante, Apellidos_Visitante;

	Visitante *Vaux;

	cout << "\nInserte nombre : " << endl;

	getline(cin, Nombre_Visitante);

	cout << "\nInserte apellidos :" << endl;

	getline(cin, Apellidos_Visitante);

	if (BD_Visitantes->buscarVisitante(Nombre_Visitante, Apellidos_Visitante,
			Vaux))

		Vaux->mostrar();

	else

		cout << "Visitante no encontrado en la base de datos." << endl;

	Vaux = NULL;

	cin.get();

}

void GastroEDI::distribuirVisitantes() {

	Visitante *Vaux;

	Restaurante *Raux;

	string Preferencia;

	Preferencias *Paux;

	int i;

	bool servido = false;

	while (BD_Visitantes->primerNA(Vaux)) {

		i = 1;

		Paux = new Preferencias();

		servido = false;

		Vaux->setAtendido(true);

		while (Vaux->getPreferencia(Preferencia)) {

			if (BD_Restaurantes->buscarRestaurante(Preferencia, Raux)) {

				if (Raux->insertarReserva(Vaux)) {

					cout << "OK: " << Vaux->getNombre() << " "
							<< Vaux->getApellidos()
							<< " atendido en el restaurante \"" << Preferencia
							<< "\"." << endl;

					Vaux->setEleccion(i);

					servido = true;

					delete Paux;

					Vaux->borrarPreferencias();

					break;

				}

				else

					cout << "No hay aforo en el Restaurante \""
							<< Raux->getNombre() << "\" para "
							<< Vaux->getComensales() << " comensales." << endl;

				Paux->insertarPreferencia(Preferencia);

			}

			else

				cout << "\nNo existe Restaurante con nombre indicado." << endl;

			i++;

		}

		if (!servido) {

			cout << "ERROR: " << Vaux->getNombre() << " "
					<< Vaux->getApellidos()
					<< " no ha podido insertarse en ninguna de sus preferencias."
					<< "\n\tSe insertará en la Base de Datos de No Atendidos"
					<< endl;

			Vaux->recuperarPreferencias(Paux);

			delete Paux;

			BD_VisitantesNA->insertarVisitante(Vaux);

		}

	}

	cout << "\n================================="
			"\n==    TODOS LOS VISITANTES     =="
			"\n==     HAN SIDO ATENDIDOS      =="
			"\n=================================" << endl;

	Vaux = NULL;

	Raux = NULL;

	cin.get();

}

void GastroEDI::consultarRestaurante() {

	string Nombre_Restaurante;

	Restaurante *Restaurante;

	cout << "\nInserte nombre : ";

	getline (cin, Nombre_Restaurante);

	if (BD_Restaurantes->buscarRestaurante(Nombre_Restaurante, Restaurante))

		Restaurante->mostrar();

	else

		cout << "Restaurante no encontrado en la base de datos." << endl;

	Restaurante = NULL;

	cin.get();

}

void GastroEDI::consultarRestaurantes() {

	string Nombre_Restaurante;

	cout << "\nInserte nombre : ";

	getline (cin, Nombre_Restaurante);

	BD_Restaurantes->buscarCoincidencia(Nombre_Restaurante);

	cin.get();

}

void GastroEDI::consultarVisitantesNA() {

	BD_VisitantesNA->mostrar();

	cin.get();

}

void GastroEDI::anularReserva() {

	string Nombre_Restaurante, Nombre_Visitante, Apellidos_Visitante;

	Visitante *Vaux;

	Restaurante *Raux;

	cout << "\nInserte nombre del Visitante: ";

	cin >> Nombre_Visitante;

	cout << "\nInserte apellidos del Visitante: ";

	cin >> Apellidos_Visitante;

	cout << "\nInserte nombre del Restaurante: ";

	cin >> Nombre_Restaurante;

	if (BD_VisitantesNA->buscarVisitante(Nombre_Visitante, Apellidos_Visitante,
			Vaux)) {

		if (Vaux->borrarPreferencia(Nombre_Restaurante))

			cout << "OK: Preferencia borrada de visitante no atendido." << endl;

		else

			cout << "ERROR: El visitante no tiene como preferencia \""
					<< Nombre_Restaurante << "\"." << endl;

	}

	else {

		if (BD_Restaurantes->buscarRestaurante(Nombre_Restaurante, Raux)) {

			if (Raux->deleteReserva(Nombre_Visitante, Apellidos_Visitante)) {

				cout << "OK: Visitante borrado del restaurante." << endl;

				BD_VisitantesNA->insertarVisitante(Vaux);

			}

			else

				cout << "ERROR: No existe visitante con ese nombre en \""
						<< Nombre_Restaurante << "\"." << endl;

		}

		else

			cout << "No relacion entre \"" << Nombre_Visitante << " "
					<< Apellidos_Visitante << "\" y \"" << Nombre_Restaurante
					<< "\"." << endl;
	}

	Vaux = NULL;

	Raux = NULL;

	cin.get();

}

void GastroEDI::valoracion() {

	BD_Restaurantes->votacion();

	cout << "\n================================="
			"\n==    TODOS LOS VISITANTES     =="
			"\n==    HAN EMITIDO SU VOTO      =="
			"\n=================================" << endl;

this->Salida_Registro();

}

void GastroEDI::restaurantemasValorado() {

	Restaurante *Raux;

	cout << "\n================================="
			"\n==        RESTAURANTE          =="
			"\n==       MÁS  VALORADO         =="
			"\n=================================" << endl;

	if (BD_Restaurantes->masValorado(Raux, MAXVALORACION, "{")) {

		Raux->mostrar();

	}

	else

		cout << "\nLista de BD_Restaurantes vacia.";

	cin.get();

}

void GastroEDI::finalizar() {

	cout << "\n================================="
			"\n==          GRACIAS            =="
			"\n==      HASTA LA PRÓXIMA       =="
			"\n=================================" << endl;

	this->Salida_Valoraciones();

	this->Salida_NoAtendidos();

	cout << "\n\nA continuación se borrarán todos los datos almacenados"
			<< endl;

	cin.get();

}
