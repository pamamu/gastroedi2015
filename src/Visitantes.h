//=================================================================================================
// Nombre      : Visitantes.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Clase Visitantes. Definición de la clase que almacena y gestiona todos los
//				 visitantes en orden de llegada. Permite la b�squedad y consulta directa de
//				 visitantes por su nombre
//=================================================================================================

#ifndef VISITANTES_H_
#define VISITANTES_H_

#include "../EDs/listapi.h"
#include "Visitante.h"

class Visitantes {

private:

	ListaPI<Visitante *> *l_visitantes; //Puntero a lista con punto de interes de punteros a Visitantes

    /* DESC:   Muestra una lista pasada por parámetro
     * PRE:    La lista debe estar creada
     * POST:   Muestra por pantalla todos los visitantes dentro de la lista
     * PARAM:  E: l -> Lista a mostrar -> ListaPI<Visitante *> *
     * RET:    -
     * COMP:   O(n)
     */

	void mostrarLista(ListaPI<Visitante *> *l);

public:

    /* DESC:   Constructor por defecto
     * PRE:    -
     * POST:   Crea una lista de visitantes
     * PARAM:  -
     * RET:    -
     * COMP:   O(1)
     */

	Visitantes();

    /* DESC:   Destructor
     * PRE:    La instancia debe estar creada
     * POST:   Vacia la lista, elimina todos sus datos y la propia lista. La lista apunta a NULL
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	~Visitantes();

    /* DESC:   Muestra todos los visitantes almacenados en la lista
     * PRE:    La instancia debe estar creada
     * POST:   Muestra por pantalla la lista de Visitantes
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void mostrar();

    /* DESC:   Inserta un visitante a la lista
     * PRE:    La instancia debe estar creada
     * POST:   Lista de visitantes con un elemento mas en la primera posicion
     * PARAM:  E: V -> Visitante a insertar en lista -> Visitante*
     * RET:    -
     * COMP:   O(1)
     */

	void insertarVisitante(Visitante *V);

    /* DESC:   Elimina un visitante de la lista de visitantes
     * PRE:    La instancia debe estar creada
     * POST:   Lista con un elemento menos -> El que indique el parametro de entrada
     * PARAM:  E: nombre -> Nombre del visitante que se quiera eliminar -> const string &
     * 		   E: apellidos -> Apellidos del visitante que se quiera eliminar -> const string &
     * 		   S: V_borrado -> Visitante que se ha eliminado de la ED -> Visitante*
     * RET:    TRUE: El visitante el nombre y apellidos especificados esta en la lista y ha sido borrado
     * 		   FALSE: El visitante el nombre y apellidos especificados NO esta en la lista y ha sido borrado
     * COMP:   O(n)
     */

	bool borrarVisitante(const string &nombre, const string &apellidos, Visitante* V_borrado);

    /* DESC:   Busca un visitante en la lista de visitantes
     * PRE:    La instancia debe estar creada
     * POST:   Si no se encuentra el Visitante, V apunta a NULL
     * PARAM:  E: nombre -> Nombre del visitante que se quiera buscar -> const string &
     * 		   E: apellidos -> Apellidos del visitante que se quiera buscar -> const string &
     * 		   S: V -> Visitante en donde se almacena el visitante dentro de la lista -> Visitante *
     * RET:    TRUE: El visitante el nombre y apellidos especificados esta en la lista
     * 		   FALSE: El visitante el nombre y apellidos especificados NO esta en la lista
     * COMP:   O(n)
     */

	bool buscarVisitante(const string &nombre, const string &apellidos, Visitante * &V);

    /* DESC:   Devuelve el primer visitante no Atendido
     * PRE:    La instancia debe estar creada
     * POST:   Devuelve el primer visitante no Atendido de la ED de todos los Visitantes
     * PARAM:  S: VisitanteNA -> Primer visitante NA -> Visitante* &
     * RET:    TRUE : Se ha encontrado un visitante no atendido
     * 		   FALSE: No se ha encontrado un visitante no atendido (todos atendidos) y VisitanteNA=NULL
     * COMP:   O(n)
     */

	bool primerNA(Visitante* &VisitanteNA);

    /* DESC:   Todos los visitantes incluidos en el aED emiten una valoración
     * PRE:    La instancia debe estar creada
     * POST:   Devuelve la valoración media de los visitantes
     * PARAM:  -
     * RET:    Devuelve la media de todas las valoraciones de los visitantes en float
     * COMP:   O(n)
     */

	float votacion();

    /* DESC:   Escribe en el flujo informacion relativa a los visitantes
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe información relativa a los visitantes de ED
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */

	void salida(ofstream &flujo_salida);



};

#endif /* VISITANTES_H_ */
