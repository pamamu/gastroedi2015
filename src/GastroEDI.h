//=================================================================================================
// Nombre      : GastroEDI.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Clase GastroEDI.Definici�n de la clase que se encarga del control y gesti�n de
//				 todas las clases y permite al usuario realizar las acciones que se muestran en
//				 el men� principal
//=================================================================================================

#ifndef GASTROEDI_H_
#define GASTROEDI_H_

#include "Restaurantes.h"
#include "Visitantes.h"
#include "VisitantesNA.h"
#include <fstream>
#include <time.h>

class GastroEDI {

	Restaurantes *BD_Restaurantes; //Puntero a BD_Restaurantes

	Visitantes *BD_Visitantes; //Puntero a BD_Visitantes

	VisitantesNA *BD_VisitantesNA; //Puntero a BD_VisitantesNA

    /* DESC:   Escribe en fichero información de restaurantes con sus valoraciones ordenadas en orden de valoración
     * PRE:    La instancia debe estar creada
     * POST:   Por cada restaurante se mostrará Nombre#dirección#aforo del día#valoración
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void Salida_Valoraciones();

    /* DESC:   Escribe en fichero información del registro diario de visitantes en cada restaurante
     * PRE:    La instancia debe estar creada
     * POST:   Por cada Restaurante se mostrará Nombre restaurante#dirección#número de tenedores#teléfono#aforo#valoración
     * 		   Por cada visitante se mostrará Nombre visitante#apellidos#ciudad de origen#número de comensales#número de elección del restaurante
     * PARAM:  -
     * RET:    -
     * COMP:   O(n²)
     */

	void Salida_Registro();

    /* DESC:   Escribe en fichero información de los visitantes que no han conseguido reserva en un restaurante
     * PRE:    La instancia debe estar creada
     * POST:   Por cada Visitante se mostrará Nombre y apellidos#provincia#número de comensales
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void Salida_NoAtendidos();

public:

	/* DESC:   Constructor por defecto
	 * PRE:    -
	 * POST:   Crea e inicializa la instancia de la clase(RestaurantesLISTA() VisitantesLISTA() VisitantesNACOLA())
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	GastroEDI();

	/* DESC:   Destructor
	 * PRE:    La instancia debe estar creada
	 * POST:   Elimina todas las ED y su contenido y libera la memoria dinamica asociada a esta instancia
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	~GastroEDI();

    /* DESC:   Crea los restaurantes y los introduce en la ED correspondiente.
     * PRE:    La instancia debe estar creada
     * POST:   Los restaurantes son creados e introducidos en la ED que los almacena
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void cargarRestaurantes();

    /* DESC:   Crea los visitantes y los introduce en la ED correspondiente.
     * PRE:    La instancia debe estar creada
     * POST:   Los visitantes son creados e introducidos en la ED que los almacena temporalmente hasta que se asignen
	 * 		   a sus respectivos restaurantes
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void cargarVisitantes();

    /* DESC:   Consulta un Visitante en la base de datos de Visitantes
     * PRE:    La instancia debe estar creada
     * POST:   Se pide información al usuario como el nombre y el apellido del visitante. Trás esto,
     * 		   se busca en la base de datos un visitante con el nombre y el apellido.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void consultarVisitante();

    /* DESC:   Distribuye los visitantes de la cola de no atendidos en restaurantes atendiendo a sus preferencias.
     * PRE:    La instancia debe estar creada
     * POST:   Se distribuyen los visitantes en los restaurantes. Si el visitante se inserta en un restaurante se
     * 		   muestra mensaje de OK, si no es capaz de insertarlo muestra mensaje de error, y si despues de recorrer
     * 		   todas sus preferencias no se puede insertar en ninguno se vuelve a insertar en la cola de no atendidos
     * 		   a través de una cola auxiliar.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n^3)
     */

	void distribuirVisitantes();

    /* DESC:   Consulta un restaurante de la base de datos de Restaurantes
     * PRE:    La instancia debe estar creada
     * POST:   Se pide información al usuario como el nombre del restaurante. Tras esto, se busca en la base de datos un restaurante
     * 		   con el nombre indicado. Si existe, muestra por pantalla toda la información relativa al restaurante, si no existe
     * 		   muestra mensaje de error.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void consultarRestaurante();

    /* DESC:   Consulta restaurantes en la base de datos de Restaurantes que se inicien por una raiz.
     * PRE:    La instancia debe estar creada
     * POST:   Se pide información al usuario como la raiz común de los visitantes a mostrar. Tras esto, se buscan los restaurantes
     * 		   que se inicien por dicha raiz y se muestran por pantalla.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void consultarRestaurantes();

    /* DESC:   Consulta y muestra información de los visitantes que no han podido ser atendidos.
     * PRE:    La instancia debe estar creada
     * POST:   Se muestran por pantalla los visitantes no atendidos y la informacion referente a las preferencias.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void consultarVisitantesNA();

    /* DESC:   Anula la reserva de un visitante
     * PRE:    La instancia debe estar creada
     * POST:   Anula la reserva de un visitante. Si el visitante o el restaurante no se encuentra en ñla base de datos se muestra mensaje de
     * 		   error. Si el restuarante y el visitante existe:
     * 		   -> Si el Visitante se encuentra en la base de datos de no atendidos se elimina la preferencia
     * 		   -> Si el visitante se encuentra en la base de datos de un restaurante, el visitante se sale del restaurante, se le restauran las preferencias
     * 		   y vuelve a la base de datos de no atendidos
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void anularReserva();

    /* DESC:   Genera una votación simulada de todos los visitantes que están en un restaurante
     * PRE:    La instancia debe estar creada
     * POST:   Se genera una valoracion aleatorio por cada visitante, que se almacena en la valoracion total del restaurante.
     * PARAM:  -
     * RET:    -
     * COMP:   O(n^2)
     */

	void valoracion();

    /* DESC:   Se muestra el restaurante mas valorado
     * PRE:    La instancia debe estar creada
     * POST:   Busca en la base de datos de los restaurante el que tenga mayor valoracion, y lo muestra por pantalla
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void restaurantemasValorado();

    /* DESC:   Elimina todos los datos almacenados en las bases de datos
     * PRE:    La instancia debe estar creada
     * POST:   Se eliminan todas las instancias de todos los objetos - se ejecuta el destructor de cada clase
     * PARAM:  -
     * RET:    -
     * COMP:   O(-)
     */

	void finalizar();





};

#endif /* GASTROEDI_H_ */
