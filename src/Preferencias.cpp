//=================================================================================================
// Nombre      : Preferencias.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementanción de la clase Preferencias
//=================================================================================================

#include "Preferencias.h"

//Métodos Privados

void Preferencias::mostrarPreferencia(const string &P, const int &orden) {

	cout << "-- Preferencia " << orden << " -> " << P << "." << endl; //Muestra el orden y la preferencia

}

void Preferencias::mostrarCola(Cola<string>* &c) {

	string paux; //Preferencia auxiliar

	Cola<string> *caux = new Cola<string>(); //Cola auxiliar

	int i = 1; //Contador auxiliar

	while (!c->vacia()) { //Mientras la cola principal no esta vacia...

		c->primero(paux); //El primer elemento se guarda en preferencia

		mostrarPreferencia(paux, i); //Se llama al m�dulo mostrarPreferencia

		caux->encolar(paux); //Encola la preferencia en la cola auxiliar

		c->desencolar(); //Desencola el primer elemento de la cola principal

		i++; //Aumenta el indice (orden de preferencia)

	}

	delete c; //Elimina la cola principal

	c = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

}

//Constructores y destructores

Preferencias::Preferencias() {

	c_preferencias = new Cola<string>(); //c_preferencias apunta a una nueva cola de string

	cout << "Clase Preferencias - Ejecutado constructor por defecto" << endl;

}

Preferencias::~Preferencias() {

	delete c_preferencias; //Elimina la cola principal

	c_preferencias = NULL; //c_preferencias apunta a NULL

	cout << "Clase Preferencias - Ejecutado destructor" << endl;

}

//Métodos Públicos

void Preferencias::mostrar() {

	mostrarCola(c_preferencias); //Se llama al módulo mostrarCola

}

void Preferencias::insertarPreferencia(const string &_Preferencia) {

	c_preferencias->encolar(_Preferencia); //Encola _Preferencia

}

bool Preferencias::borrarPreferencia(const string &_Preferencia) {

	bool borrado = false;

	string paux; //String(Preferencia) auxiliar

	Cola<string> *caux = new Cola<string>(); //Cola auxiliar

	while (!c_preferencias->vacia()) { //Mientras la cola no est� vacia

		c_preferencias->primero(paux); //El primer elemento se guarda en paux

		if (paux != _Preferencia) //Si paux es distinta que el Restaurante de entrada

			caux->encolar(paux); //Encola paux en la cola auxiliar

		else

			borrado = true;

		c_preferencias->desencolar(); //Desencola el primer elemento de la cola principal

	}

	delete c_preferencias; //Elimina la cola principal

	c_preferencias = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

	return borrado;

}

bool Preferencias::buscarPreferencia(const string &_Preferencia) {

	string paux; //String(Preferencia) auxiliar

	Cola<string> *caux = new Cola<string>(); //Cola auxiliar

	bool encontrado = false; //Bandera auxiliar

	while (!c_preferencias->vacia()) { //Mientras la cola no est� vacia

		c_preferencias->primero(paux); //El primer elemento se guarda en paux

		if (paux == _Preferencia) //Si paux es distinta que el Restaurante de entrada

			encontrado = true; //Bandera a true -> Preferencia encontrada

		c_preferencias->desencolar(); //Desencola el primer elemento de la cola principal

	}

	delete c_preferencias; //Elimina la cola principal

	c_preferencias = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

	return encontrado; //Devuelve encontrado(bandera)

}

string Preferencias::primeraPreferencia() {

	string paux; //String(Preferencia) auxiliar

	if (!c_preferencias->vacia()) { //Si la cola no esta vacia...

		c_preferencias->primero(paux); //El primer elemento se guarda en paux

		c_preferencias->desencolar(); //Desencola el primer elemento de la cola principal

	}

	else

		paux = "";

	return paux;

}

