//=================================================================================================
// Nombre      : UI.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Clase UI(User Interface).Definici�n de la clase que muestra el men� de opciones
//				  y ejecuta dicho men� hasta que el usuario desee abandonar la aplicaci�n GASTROEDI
//=================================================================================================

#ifndef UI_H_
#define UI_H_

#include "GastroEDI.h"

class UI {

private:

	GastroEDI *Gastro_EDI;

	/* DESC:   Muestra el men� con las distinta opciones de la aplicaci�n
	 * PRE:    La instancia debe estar creada
	 * POST:   Muestra el men� de opcioness de la aplicaci�n
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	int menu();

	/* DESC:   Ejecuta la opcion del men� deseada
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void ejecutar();

public:

	/* DESC:   Constructor por defecto
	 * PRE:    -
	 * POST:   Crea e inicializa la instancia de la clase(RestaurantesLISTA() VisitantesLISTA() VisitantesNACOLA())
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	UI();

	/* DESC:   Destructor
	 * PRE:    La instancia debe estar creada
	 * POST:   Elimina todas la instancia
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	~UI();
};

#endif /* UI_H_ */
