//=================================================================================================
// Nombre      : Visitantes.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementanción de la clase Visitantes
//=================================================================================================

#include "Visitantes.h"

//Métodos Privados

void Visitantes::mostrarLista(ListaPI<Visitante *> *l) {

	Visitante *v = NULL;

	int i = 1; //Contador auxiliar

	l->moverInicio();

	while (!l->finLista()) {

		l->consultar(v);

		cout << "\n_______________Visitante " << i << "_________________" << endl; //Muestra mensaje

		v->mostrar();

		l->avanzar();

		i++; //Aumenta el indice (orden de visitante)

	}

}

//Constructores y destructores

Visitantes::Visitantes() {

	l_visitantes = new ListaPI<Visitante *>(); //l_visitantes apunta a una lista lpi de punteros a Visitantes

	cout << "Clase Visitantes - Ejecutado constructor por defecto" << endl;

}


Visitantes::~Visitantes() {

	Visitante *Vaux;

	l_visitantes->moverInicio();

	while (!l_visitantes->finLista()) {

		l_visitantes->consultar(Vaux);

		l_visitantes->borrar();

		delete Vaux;
	}

	delete l_visitantes;

	l_visitantes = NULL;

	cout << "Clase Visitante - Ejecutado destructor" << endl;

}

//Métodos Públicos

void Visitantes::mostrar() {

	if (l_visitantes->estaVacia()) {

		cout <<   "========================================="
				"\n==          NO HAY VISITANTES          =="
				"\n=========================================" << endl;

	}

	else {

		cout << "========================================="
				"\n==           LISTA VISITANTES          =="
				"\n=========================================" << endl;

		mostrarLista(l_visitantes); //Llama al metodo mostrarLista pasando la l_visitantes por par�metro

	}

}

void Visitantes::insertarVisitante(Visitante *V) {

	l_visitantes->moverFinal();

	l_visitantes->avanzar();

	l_visitantes->insertar(V);

}


bool Visitantes::borrarVisitante(const string &nombre, const string &apellidos, Visitante* V_borrado) {

	Visitante *V = new Visitante(nombre, apellidos); // Visitante auxiliar con nombre y apellidos

	l_visitantes->moverInicio(); //Mueve el puntero al primer elemento de la lista

	while (!l_visitantes->finLista()) { //Recorre todos los elementos de la lista

		l_visitantes->consultar(V_borrado); //Vaux apunta al visitante de la lista

		if (V_borrado->operator ==(*V)) { //Si Vaux(Visitante) es igual al Visitante que apunta V

			l_visitantes->borrar(); //Se elimina el visitante al que apunta el puntero

			delete V; //Elimina V

			V = NULL;

			return true; //Devuelve true

		}

		l_visitantes->avanzar(); //El puntero avanza al siguiente elemento de la lista

	}

	//Si después de recorrer la lista entera no lo encuentra...

	delete V; //Elimina V

	V = NULL;

	V_borrado = NULL; //Vaux apunta a NULL

	return false; //Devuelve false

}

bool Visitantes::buscarVisitante(const string &nombre, const string &apellidos, Visitante * &V) {

	Visitante *Vaux = new Visitante(nombre, apellidos); // Visitante auxiliar con nombre y apellidos

	l_visitantes->moverInicio(); //Mueve el puntero al primer elemento de la lista

	while (!l_visitantes->finLista()) { //Recorre todos los elementos de la lista

		l_visitantes->consultar(V); //V apunta al visitante de la lista

		if (V->operator ==(*Vaux)) { //Si Vaux(Visitante) es igual al Visitante que apunta V

			delete Vaux; //Elimina Vaux

			return true; //Devuelve true

		}

		l_visitantes->avanzar(); //El puntero avanza al siguiente elemento de la lista

	}

	//Si después de recorrer la lista entera no lo encuentra...

	delete Vaux; //Elimina Vaux

	V = NULL; //V apunta a NULL

	return false; //Devuelve false
}

bool Visitantes::primerNA(Visitante* &VisitanteNA){

this->l_visitantes->moverInicio();

while(!l_visitantes->finLista()){

	this->l_visitantes->consultar(VisitanteNA);

	if(!VisitanteNA->getAtendido())

		return true;

	l_visitantes->avanzar();

}

VisitanteNA = NULL;

return false;

}

float Visitantes::votacion() {

	Visitante *Vaux;

	float i = 0, suma = 0;

	if(l_visitantes->estaVacia())

		return 0;

	l_visitantes->moverInicio();

	while (!l_visitantes->finLista()) {

		l_visitantes->consultar(Vaux);

		suma += Vaux->votar();

		i++;

		l_visitantes->avanzar();

	}

	Vaux = NULL;

	return suma/i;

}

void Visitantes::salida(ofstream &flujo_salida){

	Visitante *Vaux;

	l_visitantes->moverInicio();

	while(!l_visitantes->finLista()){

		l_visitantes->consultar(Vaux);

		Vaux->salida(flujo_salida);

		l_visitantes->avanzar();

	}

	Vaux = NULL;

}

