//=================================================================================================
// Nombre      : Restaurante.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Clase Restaurante. Definici�n de la clase que almacena y gestiona la informaci�n de
// 				 cada Restaurante y sus Comensales actuales.
//=================================================================================================

#ifndef RESTAURANTE_H_
#define RESTAURANTE_H_

#include "Visitantes.h"

const int MAXTENEDORES = 10;

class Restaurante {

private:

	string Nombre; 	//Variable tipo string que indica el nombre del restaurante
	string Direccion; //Variable tipo string que indica el la direccion del restaurante
	int NumTenedores; //Variable de tipo entero que indica el n�mero de tenedores del restaurante
	string Telefono; //Variable de tipo string que indica el n�mero de tel�fono
	int Aforo;	//Variable de tipo entero que indica el aforo del restaurante
	int Ocupacion;//Variable de tipo entero que indica la ocupacion del restaurante
	float Valoracion;//Variable de tipo entero que indica la valoracion del restaurante
	Visitantes *l_Visitantes; //Puntero a cola de visitantes que actualmente est�n en el restaurante

public:

	/* DESC:   Constructor por defecto
	 * PRE:    -
	 * POST:   Crea una instancia de Restaurante vacia (string = "", int = 0, VisitantesCOLA())
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	Restaurante();

	/* DESC:   Constructor parametrizado
	 * PRE:    -
	 * POST:   Crea una instancia de Visitante con los valores de los par�metros(int = 0, VisitantesCOLA())
	 * PARAM:  E: _Nombre -> Nombre del Visitante -> string
	 * 		   E: _Direccion -> Direccion del Restaurante-> string
	 * 		   E: _NumTenedores -> Numero de tenedores del Resturante -> int
	 * 		   E: _Telefono -> Numero de telefono del Restaurante -> int
	 * 		   E: _Aforo -> Aforo total del restaurante -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	Restaurante(string _Nombre, string _Direccion, int _NumTenedores,
			int _Aforo, string _Telefono);

	/* DESC:   Destructor
	 * PRE:    La instancia debe estar creada
	 * POST:   Elimina el restaurante y la cola de Visitantes que hay en el.
	 *         Libera la memoria dinamica asociada a esta instancia
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	~Restaurante();

	/* DESC:   Introduce el nombre al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo nombre del Restaurante es igual que el parametro de entrada
	 * PARAM:  E: _Nombre -> Nombre que se desea insertar en la instancia del Restaurante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setNombre(string _Nombre);

	/* DESC:   Introduce la direcci�n al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo direcci�n del Restaurante es igual que el parametro de entrada
	 * PARAM:  E: _Direccion -> Direccion que se desea insertar en la instancia del Restaurante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setDireccion(string _Direccion);

	/* DESC:   Introduce el numero de tenedores al Restaurante
	 * PRE:    La instancia debe estar creada y 0<=_NumTenedores<=MAXTENEDORES
	 * POST:   El atributo NumTenedores del Restaurante es igual que el parametro de entrada
	 * PARAM:  E: _NumTenedores -> Numero de tenedores que se desea insertar en la instancia del Restaurante -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setNumTenedores(int _NumTenedores);

	/* DESC:   Introduce el tel�fono al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Telefono del Restaurante es igual que el parametro de entrada
	 * PARAM:  E: _Telefono -> Numero de tel�fono que se desea insertar en la instancia del Restaurante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setTelefono(string _Telefono);

	/* DESC:   Introduce el Aforo al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Aforo del Restaurante es igual que el parametro de entrada
	 * PARAM:  E: _Aforo -> Aforo que se desea insertar en la instancia del Restaurante -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setAforo(int _Aforo);

	/* DESC:   Introduce la ocupaci�n al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Ocupaci�n del Restaurante es igual que el parametro de entrada si Aforo>=_Ocupacion>=0
	 * PARAM:  E: _Ocupaci�n -> Ocupaci�n que se desea insertar en la instancia del Restaurante -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setOcupacion(int _Ocupacion);

	/* DESC:   Introduce la valoraci�n al Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   todo
	 * PARAM:  E: _Valoracion -> Valoracion que se desea insertar en la instancia del Restaurante -> float
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setValoracion(float _Valoracion);

	/* DESC:   Devuelve el nombre del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    string : Nombre del Restaurante
	 * COMP:   O(1)
	 */

	string getNombre() const;

	/* DESC:   Devuelve la direccion del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    string : Dirección del Restaurante
	 * COMP:   O(1)
	 */

	string getDireccion() const;

	/* DESC:   Devuelve el numero de tenedores del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    int : N�mero de Tenedores
	 * COMP:   O(1)
	 */

	int getNumTenedores() const;

	/* DESC:   Devuelve el tel�fono del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    string : Telefono del Restaurante
	 * COMP:   O(1)
	 */

	string getTelefono() const;

	/* DESC:   Devuelve el aforo total del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    int : Aforo del Restaurante
	 * COMP:   O(1)
	 */

	int getAforo() const;

	/* DESC:   Devuelve la ocupacion(numero de comensales actuales) del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    int : Ocupacion del Restaurante
	 * COMP:   O(1)
	 */

	int getOcupacion() const;

	/* DESC:   Devuelve la valoraci�n del Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    float : Valoración del Restaurante
	 * COMP:   O(1)
	 */

	float getValoracion() const;

	/* DESC:   Devuelve la cantidad de sitios libres en el Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    int : Sitios libres en el Restaurante
	 * COMP:   O(1)
	 */

	int getSitiosLibres() const;

	/* DESC:   Inserta una reserva de un visitante en el Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   Se inserta en el restaurante un visitante con una serie de comensales.
	 * 		   La ocupaci�n aumenta en tantos comensales como tenga la reserva.
	 * PARAM:  E: _Visitante -> Visitante que se quiere insertar en el restaurante.
	 * RET:    TRUE: La reserva se ha insertado con �xito
	 * 		   FALSE: La reserva no ha podido insertarse debido a que no hay sitios libres para tantos comensales
	 * COMP:   O(1)
	 */

	bool insertarReserva(Visitante* _Visitante);

	/* DESC:   Borrar reserva de un Visitante en el Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   Se elimina del restaurante un visitante con una serie de comensales.
	 * 		   La ocupación disminuye en tantos comensales como tenga la reserva.
	 * PARAM:  E: Nombre_Visitante -> Visitante que quiere borrarse del Restaurante -> const string &
	 * RET:    TRUE: La reserva se ha borrado con éxito
	 * 		   FALSE: La reserva no ha podido borrado debido a que no existe la reserva
	 * COMP:   O(n)
	 */

	bool deleteReserva(const string &Nombre_Visitante,
			const string &Apellidos_Visitante);

	/* DESC:   Muestra la informaci�n relativa a un restaurante, desde sus datos como los de sus comensales actuales(opcional)
	 * PRE:    La instancia debe estar creada
	 * POST:   Muestra por pantalla informaci�n del restaurante. Dependiendo de la elecci�n del usuario se mostrar� informacion de los comensales
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void mostrar() const;

    /* DESC:   Escribe en el flujo informacion relativa a los visitantes
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe Nombre restaurante#dirección#número de tenedores#teléfono#aforo#valoración
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */

	void salida(ofstream &flujo_salida);

	/* DESC:   Votan todos los visitantes del restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   Cada visitante con su voto, lo introduce a la valoracion del Restaurante
	 * PARAM:  - : -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void votacion();

	/* DESC:   Sobrecarga del operador de comparaci�n ==
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  E: R -> Restaurante con el que vamos a comparar la instancia -> Restaurante
	 * RET:    TRUE: Nombre y Direcci�n de ambos son iguales
	 * 		   FALSE: Nombre y Direcci�n de ambos NO son iguales
	 * COMP:   O(1)
	 */

	bool operator ==(const Restaurante &per) const;

};

#endif /* RESTAURANTE_H_ */
