//=================================================================================================
// Nombre      : UI.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementanción de la clase UI
//=================================================================================================

#include <iostream>

#include "UI.h"

using namespace std;

//Métodos privados

void UI::ejecutar() {

	int opcion;
	bool salir = false;
	do {
		opcion = menu();

		switch (opcion) {

		case 1:

			Gastro_EDI->cargarRestaurantes();

			Gastro_EDI->cargarVisitantes();

			break;
		case 2:
			Gastro_EDI->consultarVisitante();
			break;
		case 3:
			Gastro_EDI->distribuirVisitantes();
			break;
		case 4:
			Gastro_EDI->consultarRestaurante();
			break;
		case 5:
			Gastro_EDI->consultarRestaurantes();
			break;
		case 6:
			Gastro_EDI->consultarVisitantesNA();
			break;
		case 7:
			Gastro_EDI->anularReserva();
			break;
		case 8:
			Gastro_EDI->valoracion();
			break;
		case 9:
			Gastro_EDI->restaurantemasValorado();
			break;
		case 10:
			Gastro_EDI->finalizar();
			salir = true;
			break;
		default:
			cout << "ERROR en la opcion de menu" << endl;
			break;
		}

	} while (!salir);
}

int UI::menu() {

	int opcion;

//	system("clear");

	do {
		cout << endl << endl;
		cout << "___________________GASTROEDI_2015_________________" << endl;
		cout << "_________________PABLO MACIAS MUÑOZ_______________" << endl;
		cout << "__________________ MENU PRINCIPAL ________________" << endl
				<< endl;
		cout << "   1.  Carga de datos." << endl;
		cout << "   2.  Consulta de un visitante y sus peticiones." << endl;
		cout << "   3.  Distribución.                             " << endl;
		cout << "   4.  Consulta de un restaurante.               " << endl;
		cout << "   5.  Consulta de restaurantes.                 " << endl;
		cout << "   6.  Consulta de visitantes no servidos.       " << endl;
		cout << "   7.  Anulación de reserva.                     " << endl;
		cout << "   8.  Valoración.                               " << endl;
		cout << "   9.  Restaurante mejor valorado.               " << endl;
		cout << "   10. Finalizar.                                " << endl;
		cout << "Opción:  ";
		cin >> opcion;
		cin.ignore();

	} while ((opcion < 0) || (opcion > 11));

	return opcion;
}

//Constructores y destructores

UI::UI() {

	Gastro_EDI = new GastroEDI();

	ejecutar();

}

UI::~UI() {


}

int main() {
	cout
			<< "\n-------->>   Bienvenidos a la aplicación GastroEDI2015  <<--------";
	cout
			<< "\n------------------------------------------------------------------";

UI ui;


}
