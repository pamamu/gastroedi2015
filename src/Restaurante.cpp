//=================================================================================================
// Nombre      : Restaurante.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripci�n : Implementanci�n de la clase Restaurante
//=================================================================================================

#include "Restaurante.h"

//COnstructores y destructores

Restaurante::Restaurante() {

	Nombre = "";

	Direccion = "";

	NumTenedores = 0;

	Telefono = "";

	Aforo = 0;

	Ocupacion = 0;

	Valoracion = 0;

	l_Visitantes = new Visitantes();

	cout << "Clase Restaurante - Ejecutado constructor por defecto" << endl;

}

Restaurante::Restaurante(string _Nombre, string _Direccion, int _NumTenedores, int _Aforo, string _Telefono) {

	Nombre = _Nombre;

	Direccion = _Direccion;

	NumTenedores = _NumTenedores;

	Telefono = _Telefono;

	Aforo = _Aforo;

	Ocupacion = 0;

	Valoracion = 0;

	l_Visitantes = new Visitantes();

	cout << "Clase Restaurante - Ejecutado constructor parametrizado" << endl;

}

Restaurante::~Restaurante() {

	delete l_Visitantes;

	l_Visitantes = NULL;

	cout << "Clase Restaurante - Ejecutado destructor" << endl;

}

//Métodos Públicos

void Restaurante::setNombre(string _Nombre) {

	Nombre = _Nombre;

}

void Restaurante::setDireccion(string _Direccion) {

	Direccion = _Direccion;

}

void Restaurante::setNumTenedores(int _NumTenedores) {

	if (_NumTenedores >= 0 && _NumTenedores <= MAXTENEDORES)

		NumTenedores = _NumTenedores;

}

void Restaurante::setTelefono(string _Telefono) {

	Telefono = _Telefono;

}

void Restaurante::setAforo(int _Aforo) {

	if (_Aforo >= 0)

		Aforo = _Aforo;

}

void Restaurante::setOcupacion(int _Ocupacion) {

	if (Aforo >= _Ocupacion && _Ocupacion >= 0)

		Ocupacion = _Ocupacion;

}

void Restaurante::setValoracion(float _Valoracion) {

	if (_Valoracion >= 0 && _Valoracion <= MAXVALORACION)

		Valoracion = _Valoracion;

}

string Restaurante::getNombre() const {

	return Nombre;

}
string Restaurante::getDireccion() const {

	return Direccion;

}

int Restaurante::getNumTenedores() const {

	return NumTenedores;

}

string Restaurante::getTelefono() const {

	return Telefono;

}
int Restaurante::getAforo() const {

	return Aforo;

}

int Restaurante::getOcupacion() const {

	return Ocupacion;

}

float Restaurante::getValoracion() const {

	return Valoracion;

}

int Restaurante::getSitiosLibres() const {

	return (Aforo - Ocupacion);

}

bool Restaurante::insertarReserva(Visitante* _Visitante) {

	if (_Visitante->getComensales() > this->getSitiosLibres())

		return false;

	this->l_Visitantes->insertarVisitante(_Visitante);

	Ocupacion += _Visitante->getComensales();

	return true;

}

bool Restaurante::deleteReserva(const string &Nombre_Visitante, const string &Apellidos_Visitante){

	Visitante *Visitante_borrado;

	if(l_Visitantes->borrarVisitante(Nombre_Visitante, Apellidos_Visitante, Visitante_borrado)){

		Ocupacion-=Visitante_borrado->getComensales();

		Visitante_borrado->setAtendido(false);

		Visitante_borrado = NULL;

		return true;

	}

	Visitante_borrado = NULL;

	return false;

}

void Restaurante::mostrar() const {

	string eleccion;

	cout << "\n\nRestaurante: " << endl;

	cout << "==============================="
			<< "\n-- Nombre - " << Nombre
			<< "\n-------------------------------"
			<< "\n-- Direcccion - " << Direccion
			<< "\n-------------------------------"
			<< "\n-- Nº Tenedores - " << NumTenedores << "/" << MAXTENEDORES
			<< "\n-------------------------------"
			<< "\n-- Aforo - " << Aforo
			<< "\n-------------------------------"
			<< "\n-- Ocupacion - " << Ocupacion
			<< "\n-------------------------------"
			<< "\n-- Teléfono - " << Telefono
			<< "\n-------------------------------"
			<< "\n-- Valoración - " << Valoracion
			<< "\n===============================" << endl;

		cout << "\n==============================="
				"\n== Comensales actuales:"
				"\n===============================" << endl;

		l_Visitantes->mostrar();

}

void Restaurante::salida(ofstream &flujo_salida) {

	flujo_salida << Nombre << "#" << Direccion << "#" << NumTenedores << "#"
			<< Telefono << "#" << Aforo << "#" << Valoracion << endl;

	l_Visitantes->salida(flujo_salida);

}

void Restaurante::votacion() {

	Valoracion = l_Visitantes->votacion();

}

bool Restaurante::operator ==(const Restaurante &per) const {

	return (Nombre == per.Nombre && Direccion == per.Direccion);

}
