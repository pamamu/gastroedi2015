//=================================================================================================
// Nombre      : Preferencias.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Clase Preferencias. Definición de la clase que almacena y gestiona las
//               preferencias. Permite la consulta directa de preferencias,
//=================================================================================================

#ifndef PREFERENCIAS_H_
#define PREFERENCIAS_H_

#include <iostream>
#include <cstring>

using namespace std;

#include "../EDs/cola.h"

class Preferencias {

private:

	Cola<string> *c_preferencias; //Puntero a cola de string

    /* DESC:   Muestra una preferencia con su n�mero de orden
     * PRE:    -
     * POST:   Muestra por pantalla la preferencia y su orden
     * PARAM:  E: P -> Preferencia que se quiere mostrar -> const string &
     * 		   E: orden -> Indica el orden de preferencia -> const int &
     * RET:    -
     * COMP:   O(1)
     */

	void mostrarPreferencia(const string &P, const int &orden);


    /* DESC:   Muestra una cola pasada por par�metro
     * PRE:    La cola debe estar creada
     * POST:   Muestra por pantalla todas las preferencias dentro de la cola
     * PARAM:  E/S: c -> Cola a mostrar -> Cola<string> *
     * RET:    -
     * COMP:   O(n)
     */

	void mostrarCola(Cola<string>* &c);

public:

    /* DESC:   Constructor por defecto
     * PRE:    -
     * POST:   Crea una cola de strings vacia
     * PARAM:  -
     * RET:    -
     * COMP:   O(1)
     */

	Preferencias();


    /* DESC:   Destructor
     * PRE:    La instancia debe estar creada
     * POST:   Vacia la cola, elimina todos sus datos y la propia cola. La cola apunta a NULL.
     *         Libera la memoria dinamica asociada a esta instancia
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	~Preferencias();

    /* DESC:   Muestra todas las preferencias almacenadas
     * PRE:    La instancia debe estar creada
     * POST:   Muestra por pantalla las cola de Preferencias
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void mostrar();

    /* DESC:   Inserta una preferencia a la cola
     * PRE:    La instancia debe estar creada
     * POST:   Cola con un elemento mas en la ultima posici�n
     * PARAM:  E: _Preferencia -> Preferencia a insertar en cola -> const string &
     * RET:    -
     * COMP:   O(1)
     */

	void insertarPreferencia(const string &_Preferencia);

    /* DESC:   Elimina una preferencia en la cola
     * PRE:    La instancia debe estar creada
     * POST:   Cola con un elemento menos -> El que indique el parametro de entrada
     * PARAM:  E: _Preferencia -> Preferencia a Restaurante que se quiera eliminar -> const string &
     * RET:    TRUE: La preferencia al restaurante esta en la cola y se ha eliminado
     * 		   FALSE: La preferencia al Restaurante no está en la cola y no se elimina
     * COMP:   O(n)
     */

	bool borrarPreferencia(const string &_Preferencia);

    /* DESC:   Busca una preferencia en la cola
     * PRE:    La instancia debe estar creada
     * POST:   -
     * PARAM:  E: _Preferencia -> Preferencia a Restaurante que se quiera buscar -> const string &
     * RET:    TRUE: La preferencia al restaurante esta en la cola
     * 		   FALSE: La preferencia al restaurate NO esta en la cola
     * COMP:   O(n)
     */

	bool buscarPreferencia(const string &_Preferencia);

    /* DESC:   Devuelve la primera preferencia
     * PRE:    La instancia debe estar creada
     * POST:   Devuelve y elimina la primera preferencia de la cola
     * PARAM:  -
     * RET:    String : Primera preferencia. Si la cola esta vacia devuelve ""
     * COMP:   O(1)
     */

	string primeraPreferencia();

};

#endif /* PREFERENCIAS_H_ */
