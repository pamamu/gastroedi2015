//=================================================================================================
// Nombre      : Visitante.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Clase Visitante. Definicó�n de la clase que almacena y gestiona la información de
// 				 cada visitante y sus preferencias.
//=================================================================================================

#ifndef VISITANTE_H_
#define VISITANTE_H_

#include <string>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <fstream>

#include "../Misc/genaleatorios.h"
#include "Preferencias.h"

const int MAXVALORACION = 10;

using namespace std;

class Visitante {

private:

	string Nombre; 	//Variable de tipo string que indica el nombre del visitante
	string Apellidos;//Variable de tipo string que indica el nombre del visitante
	string Ciudad;//Variable de tipo string que indica la ciudad de origen del Visitante
	int Comensales;	//Variable de tipo entero que indica de cuantos comensales va a ser la reserva
	bool Atendido; //Variable de tipo bool que indica si el visitante ha sido Atendido o no
	int Eleccion; //variable de tipo int que indica que elección ha sido la elegida(preferencias)
	Preferencias *BD_Preferencias; //Puntero a Preferencias donde se almacenar�n las preferencias

public:

	/* DESC:   Constructor por defecto
	 * PRE:    -
	 * POST:   Crea una instancia de Visitante vacia (string = "", int = 0, PreferenciasCOLA())
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	Visitante();

	/* DESC:   Constructor parametrizado
	 * PRE:    -
	 * POST:   Crea una instancia de Visitante con los valores de los par�metros(string = "" int = 0 PreferenciasCOLA())
	 * PARAM:  E: _Nombre -> Nombre del Visitante -> string
	 * 		   E: _Apellidos -> Apellidos del Visitante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	Visitante(string _Nombre, string _Apellidos);

	/* DESC:   Constructor parametrizado
	 * PRE:    -
	 * POST:   Crea una instancia de Visitante con los valores de los par�metros(PreferenciasCOLA())
	 * PARAM:  E: _Nombre -> Nombre del Visitante -> string
	 * 		   E: _Apellidos -> Apellidos del Visitante -> string
	 * 		   E: _Ciudad -> Ciudad del VIsitante -> string
	 * 		   E: _Comensales -> N� de Comensales de la reserva -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	Visitante(string _Nombre, string _Apellidos, string _Ciudad, int _Comensales);

	/* DESC:   Destructor
	 * PRE:    La instancia debe estar creada
	 * POST:   Elimina el visitante y la cola de Preferencias que hay en el.
	 *         Libera la memoria dinamica asociada a esta instancia
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	~Visitante();

	/* DESC:   Introduce el Nombre al Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Nombre del Visitante es igual que el parametro de entrada
	 * PARAM:  E: _Nombre -> Nombre que se desea insertar en Visitante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setNombre(string _Nombre);

	/* DESC:   Introduce los Apellidos al Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Apellidos del Visitante es igual que el parametro de entrada
	 * PARAM:  E: _Apellidos -> Apellidos que se desea insertar en Visitante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setApellidos(string _Apellidos);

	/* DESC:   Introduce la Ciudad de origen al Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Ciudad del Visitante es igual que el parametro de entrada
	 * PARAM:  E: _Ciudad -> Ciudad que se desea insertar en Visitante -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setCiudad(string _Ciudad);

	/* DESC:   Introduce el numero de Comensales de la reserva al Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   El atributo Comensales del Visitante es igual que el parametro de entrada
	 * PARAM:  E: _Comensales -> Comensales que se desea insertar en Visitante -> int
	 * RET:    -
	 * COMP:   O(1)
	 */

	void setComensales(int _Comensales);

    /* DESC:   Introduce si un visitante esta atendido o no
     * PRE:    La instancia debe estar creada
     * POST:   Modifica el atributo de Atendido
     * PARAM:  E : _Atendido -> Indica si el visitante se ha atendido o no -> bool
     * RET:    -
     * COMP:   O(n)
     */

	void setAtendido(bool _Atendido);

    /* DESC:   Introduce si un visitante el orden de eleccion del restaurante asignado
     * PRE:    La instancia debe estar creada
     * POST:   Modifica el atributo de Eleccion
     * PARAM:  E : _Eleccion -> Indica el orden de eleccion del restaurante asignado -> bool
     * RET:    -
     * COMP:   O(n)
     */

	void setEleccion(int _Eleccion);

	/* DESC:   Devuelve el Nombre del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  S: _Nombre -> Almacena el Nombre del Visitante -> string
	 * RET:    string : Nombre del visitante
	 * COMP:   O(1)
	 */

	string getNombre() const;

	/* DESC:   Devuelve los Apellidos del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    string : Apellidos del visitante
	 * COMP:   O(1)
	 */

	string getApellidos() const;

	/* DESC:   Devuelve la Ciudad del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    string : Ciudad del Visitante
	 * COMP:   O(1)
	 */

	string getCiudad() const;

	/* DESC:   Devuelve el numero de Comensales de la reserva del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    int : Numero de Comensales en la reserva
	 * COMP:   O(1)
	 */

	int getComensales() const;

    /* DESC:   Devuelve si el visitante esta atendido o no
     * PRE:    La instancia debe estar creada
     * POST:   -
     * PARAM:  -
     * RET:    TRUE -> El visitante esta atendido
     * 		   FALSE -> El visitante no está atendido
     * COMP:   O(1)
     */

	bool getAtendido() const;

    /* DESC:   Devuelve el orden de eleccion del restaurante asignado
     * PRE:    La instancia debe estar creada
     * POST:   -
     * PARAM:  -
     * RET:    int : Orden de eleccion del restaurante asignado
     * COMP:   O(1)
     */

	int getEleccion() const;

	/* DESC:   Inserta una preferencia a un Restaurante
	 * PRE:    La instancia debe estar creada
	 * POST:   La cola de preferencias con un elemento mas.
	 * PARAM:  E: _Restaurante -> Indica el restaurante de prefencia -> string
	 * RET:    -
	 * COMP:   O(1)
	 */

	void insertarPreferencia(string _Restaurante);

	/* DESC:   Devuelve la primera preferencia
	 * PRE:    La instancia debe estar creada
	 * POST:   La cola de preferencias tiene un elemento menos (preferencia devuelta)
	 * PARAM:  S: _Restaurante -> Almaacena el restaurante de la primera preferencia
	 * RET:    TRUE: La preferencia devuelta es correcta
	 * 		   FALSE: No hay prefencias en la cola -> _Restaurante = ""
	 * COMP:   O(1)
	 */

	bool getPreferencia(string &_Restaurante) const;

    /* DESC:   Borra una preferencia del Visitante
     * PRE:    La instancia debe estar creada
     * POST:   La cola de preferencias tiene un elemento menos (Preferencia entrante) si existe en la cola.
     * PARAM:  E : _Restaurante -> Nombre del restaurante de la preferencia que se quiere borrar
     * RET:    TRUE : La preferencia está en la cola y se elimina de ella
     * 		   FALSE : La preferencia no existe en la cola
     * COMP:   O(n)
     */

	bool borrarPreferencia(const string &_Restaurante);

    /* DESC:   Recuper las Preferencias a través de una estrucitra de Preferencias Auxiliar
     * PRE:    La instancia debe estar creada
     * POST:   La cola de preferencias toma los valores de la cola de entrada, de la cual se borran todos los valores
     * PARAM:  E : PreAux -> Estrucutra de Preferencias auxiliar -> Preferencias *
     * RET:    -
     * COMP:   O(n)
     */

	void recuperarPreferencias(Preferencias *PreAux);

	/* DESC:   Borra todas las preferencias del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   La cola de preferencias se vacia
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void borrarPreferencias();

    /* DESC:   Genera un voto de un visitante
     * PRE:    La instancia debe estar creada
     * POST:   -
     * PARAM:  -
     * RET:    int : Voto aleatorio entre 0 y MAXVALORACION
     * COMP:   O(1)
     */

	int votar();

	/* DESC:   Sobrecarga del operador de comparaci�n ==
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  E: per -> Visitante con el que vamos a comparar la instancia -> Visitante
	 * RET:    TRUE: Nombre y Apellidos de ambos son iguales
	 * 		   FALSE: Nombre y Apellidos de ambos NO son iguales
	 * COMP:   O(1)
	 */

	bool operator ==(const Visitante &per);

	/* DESC:   Muestra la informacion del Visitante
	 * PRE:    La instancia debe estar creada
	 * POST:   -
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void mostrar();

    /* DESC:   Escribe en el flujo informacion relativa al visitante
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe Nombre visitante#apellidos#ciudad de origen#número de comensales#número de elección del restaurante
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(1)
     */

	void salida(ofstream &flujo_salida);

    /* DESC:   Escribe en el flujo informacion relativa al visitante
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe Nombre y apellidos#provincia#número de comensales
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(1)
     */

	void salidaNA(ofstream &flujo_salida);

};

#endif /* VISITANTE_H_ */
