//=================================================================================================
// Nombre      : VisitantesNA.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Clase VisitantesNA. Definición de la clase que almacena y gestiona los
//				 visitantes no atendidos en orden de reserva. Permite la consulta directa de
//				 visitantes por su nombre.
//=================================================================================================

#ifndef VISITANTESNA_H_
#define VISITANTESNA_H_

#include "../EDs/cola.h"
#include "Visitante.h"

class VisitantesNA{

private:

	Cola<Visitante *> *visitantes_NA; //Puntero a cola de punteros a Visitantes

    /* DESC:   Muestra una cola pasada por par�metro
     * PRE:    La cola debe estar creada
     * POST:   Muestra por pantalla todas los visitantes dentro de la cola
     * PARAM:  E: c -> Cola a mostrar -> Cola<Visitante *> *
     * RET:    -
     * COMP:   O(n)
     */

	void mostrarCola(Cola<Visitante *> *&c);

    /* DESC:   Escribe en el flujo informacion relativa a los visitantes de la cola de entrada
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe información relativa a los visitantes de ED
     * 		   E: c -> Cola que se desea escribir en el flujo -> Cola<Visitante *> *&
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */

	void salidaCola(Cola<Visitante *> *&c, ofstream &flujo_salida);

public:

    /* DESC:   Constructor por defecto
     * PRE:    -
     * POST:   Crea una cola de visitantes no atendidos vacia
     * PARAM:  -
     * RET:    -
     * COMP:   O(1)
     */

	VisitantesNA();

    /* DESC:   Destructor
     * PRE:    La instancia debe estar creada
     * POST:   Vacia la cola, elimina todos sus datos y la propia cola. La cola apunta a NULL.
     *         Libera la memoria dinamica asociada a esta instancia
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	~VisitantesNA();

    /* DESC:   Muestra todos los visitantes almacenados en la cola
     * PRE:    La instancia debe estar creada
     * POST:   Muestra por pantalla las cola de Visitantes no atendidos
     * PARAM:  -
     * RET:    -
     * COMP:   O(n)
     */

	void mostrar();

    /* DESC:   Inserta un visitante a la cola de no atendidos
     * PRE:    La instancia debe estar creada
     * POST:   Cola de no atendidos con un elemento mas en la ultima posici�n
     * PARAM:  E: V -> Visitante a insertar en cola -> Visitante*
     * RET:    -
     * COMP:   O(1)
     */

	void insertarVisitante(Visitante *V);

    /* DESC:   Elimina un visitante de la cola de no atendidos
     * PRE:    La instancia debe estar creada
     * POST:   Cola de no atendidos con un elemento menos -> El que indique el parametro de entrada
     * PARAM:  E: V -> Visitante que se quiera eliminar -> Visitante*
     * RET:    -
     * COMP:   O(n)
     */

	void borrarVisitante(const string &nombre, const string &apellidos);

    /* DESC:   Busca un visitante en la cola de no atendidos
     * PRE:    La instancia debe estar creada
     * POST:   -
     * PARAM:  E: V -> Visitante que se quiera buscar -> Visitante*
     * RET:    TRUE: El visitante esta en la cola de no atendidos
     * 		   FALSE: El visitante NO esta en la cola de no atendidos
     * COMP:   O(n)
     */

	bool buscarVisitante(const string &nombre, const string &apellidos, Visitante * &V);

    /* DESC:   Extrae el primer Visitante de la cola
     * PRE:    La instancia debe estar creada
     * POST:   Si la cola esta vacia devuelve V=NULL
     * PARAM:  S : V -> Primer visitante de la cola
     * RET:    -
     * COMP:   O(1)
     */

	void primerVisitante(Visitante * &V);

    /* DESC:   Escribe en el flujo informacion relativa a los visitantes
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe información relativa a los visitantes de ED
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */

	void salida(ofstream &flujo_salida);

};

#endif /* VISITANTESNA_H_ */
