//=================================================================================================
// Nombre      : Restaurantes.h
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Muñoz
// Descripción : Clase Restaurantes. Definición de la clase que almacena y gestiona todos los
//				 restaurantes en orden alfabético. Permite la búsquedad y consulta directa de
//				 restaurantes por su nombre.
//=================================================================================================

#ifndef RESTAURANTES_H_
#define RESTAURANTES_H_

#include "Restaurante.h"
#include "../EDs/arbol.h"

class Restaurantes {

private:

	class CmpRestaurante {

	public:

		/* DESC:   Sobrecarga del operador ()
		 * PRE:    -
		 * POST:   Compara el nombre del primer Restaurante con el segundo tantos caracteres como tenga el nombre del Res1
		 * PARAM:  E: Res1 -> Restaurante a comparar -> Restaurante*
		 * 		   E: Res2 -> Restaurante con el que se compara -> Restaurante*
		 * RET:    <0 -> Res1 es "menor" que Res2
		 * 		   0 -> Res1 y Res2 tienen el mismo nombre
		 * 		   >0 -> Res1 es "mayor" que Res2
		 * COMP:   O(1)
		 */

		int operator()(const Restaurante* Res1, const Restaurante* Res2) const {

			return (Res1->getNombre().compare(Res2->getNombre()));

		}

	};

	Arbol<Restaurante *, CmpRestaurante> *restaurantes;

	/* DESC:   Muestra un arbol pasado por parámetro
	 * PRE:    El arbol debe estar creado
	 * POST:   Muestra por pantalla todos los restaurantes dentro de la lista
	 * PARAM:  E: l -> Lista a mostrar -> ListaPI<Restaurante *> *
	 * RET:    -
	 * COMP:   O(n)
	 */

	void mostrarArbolOrden(Arbol<Restaurante *, CmpRestaurante> *arbol);

    /* DESC:   Escribe en el flujo informacion relativa a los restaurantes del arbol
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe información relativa a los restaurantes de ED
     * 		   E: arbol -> Arbol que se desea escribir en el flujo -> Cola<Visitante *> *&
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */

	void salidaArbol(Arbol<Restaurante *, CmpRestaurante> *arbol,
			ofstream &flujo_salida);

    /* DESC:   Realiza la votación en todos los restaurantes del arbol
     * PRE:    La instancia debe estar creada
     * POST:   Todos los Restaurantes almacenan sus valoración
     * 		   E: arbol -> Arbol que se desea escribir en el flujo -> Cola<Visitante *> *&
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n)
     */


	void votarArbol(Arbol<Restaurante *, CmpRestaurante> *arbol);

	/* DESC:   Busca en un arbol un restaurante con mayor la mayor valoracion siendo menor que valmax
	 * PRE:    El arbol debe estar creado
	 * POST:   todo
	 * PARAM:  E: arbol -> Arbol donde se desea buscar -> LArbol<Restaurante *, CmpRestaurante>*
	 * 		   E: valmax -> Valor máximo que puede alcanzar la valoracion del restaurante -> int
	 * RET:    TRUE: Hay un Restaurante con valoracion máxima entre 0 y valmax
	 * 		   FALSE: Hay un Restaurante con valoración máxima entre
	 * COMP:   O(n)
	 */

	bool buscarenArbol_Val(Arbol<Restaurante *, CmpRestaurante> *arbol,
			Restaurante* &Raux, float valmax, const string &smax);

	/* DESC:   Busca en un arbol un restaurante con la misma raiz en el nombre que la dada
	 * PRE:    El arbol debe estar creado
	 * POST:   Muestra por pantalla los restaurantes con la misma raiz que la dada
	 * PARAM:  E: arbol -> Arbol donde se desea buscar los restaurantes -> Arbol<Restaurante *, CmpRestaurante>
	 * 		   E: raiz -> Raiz para buscar los restaurantes -> const string &
	 * RET:    -
	 * COMP:   O(log(n))
	 */

	bool buscarenArbol_Raiz(Arbol<Restaurante *, CmpRestaurante> *arbol,
			const string &raiz);

	/* DESC:   Busca en un arbol un restaurante con el mismo nombre que el dado
	 * PRE:    El arbol debe estar creado
	 * POST:   Devuelve el restaurante con el mismo nombre
	 * PARAM:  E: arbol -> Arbol donde se desea buscar los restaurantes -> Arbol<Restaurante *, CmpRestaurante>
	 * 		   E: nombre -> Nombre para buscar los restaurantes -> const string &
	 * 		   S: Raux -> Restaurante donde se alamcena el restaurante con el mismo nombre si se encuentra en el arbol
	 * RET:    TRUE: Ha encontrado un restaurante con el mismo nombre
	 * 	 	   FALSE: NO Ha encontrado un restaurante con el mismo nombre
	 * COMP:   O(log(n))
	 */

	bool buscarenArbol_Nom(Arbol<Restaurante *, CmpRestaurante> *arbol,
			Restaurante* &Raux, const string &nombre);

public:

	/* DESC:   Constructor por defecto
	 * PRE:    -
	 * POST:   Crea un arbol de restaurantes
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(1)
	 */

	Restaurantes();

	/* DESC:   Destructor
	 * PRE:    La instancia debe estar creada
	 * POST:   Vacia el arbol, elimina todos sus datos y el propio. El arbol apunta a NULL.
	 *         Libera la memoria dinamica asociada a esta instancia.
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	~Restaurantes();

	/* DESC:   Muestra todos los restaurantes almacenados en el arbol
	 * PRE:    La instancia debe estar creada
	 * POST:   Muestra por pantalla el arbol de Restaurantes ordenados alfabeticamente
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void mostrar();

	/* DESC:   Inserta un restaurante en el arbol
	 * PRE:    La instancia debe estar creada
	 * POST:   Arbol de restaurantes con un elemento mas ordenado alfabeticamente.
	 * PARAM:  E: Res -> Restaurante a insertar en lista -> const Restaurante*
	 * RET:    -
	 * COMP:   O(log(n))
	 */

	void insertarRestaurante(Restaurante* Res);

	/* DESC:   Elimina un restaurante del arbol de restaurantes
	 * PRE:    La instancia debe estar creada
	 * POST:   Arbol con un elemento menos -> El que indique el parametro de entrada. El arbol se reordena.
	 * PARAM:  E: nombre -> Nombre del restaurante que se quiera eliminar -> const string &
	 * RET:    -
	 * COMP:   O(log(n))
	 */

	void borrarRestaurante(const string &nombre);

	/* DESC:   Busca un visitante en el arbol de restaurantes
	 * PRE:    La instancia debe estar creada
	 * POST:   Si no se encuentra el Restaurante,
	 * PARAM:  E: nombre -> Nombre del restaurante que se quiera buscar -> const string &
	 * 		   S: R -> Restaurante encontrado -> Resraurante*
	 * RET:    TRUE: El restaurante con nombre con el nombre especificado esta en la lista
	 * 		   FALSE: El restaurante con nombre con el nombre especificado NO esta en la lista
	 * COMP:   O(todo)
	 */

	bool buscarRestaurante(const string &nombre, Restaurante* &R);

	/* DESC:   Busca un restaurante con una raiz
	 * PRE:    La instancia debe estar creada
	 * POST:   Busca en los restaurante, aquellos que su nombre sea igual que el parámetro de entrada. SI es igual lo muestra.
	 * PARAM:  E: raiz -> Raiz para buscar Restaurantes -> const string &
	 * RET:    -
	 * COMP:   O(log(n))
	 */

	void buscarCoincidencia(const string &raiz);

	/* DESC:   Todos los visitantes de todos los restaurantes votan
	 * PRE:    La instancia debe estar creada
	 * POST:   Se manda a todos los Restaurantes a que voten todos los visitantes
	 * PARAM:  -
	 * RET:    -
	 * COMP:   O(n)
	 */

	void votacion();

	/* DESC:   Devuelve el Restaurante mas valorado
	 * PRE:    La instancia debe estar creada y en la ED no hay restaurantes con la misma valoración
	 * POST:   Busca en todos los restaurantes y devuelve el Restaurante mas valorado por debajo del límite valmax
	 * PARAM:  E: valmax -> Valor máximo que puede tomar el restaurante más valorado encontrado -> int
	 * 		   E: smax -> String máximo que puede tomar el restaurante más valorado encontrado -> float
	 * RET:    TRUE: Existe algún Restaurante que tenga una valoración por debajo del valor máximo
	 * 		   FALSE: No existe ningún Restaurante que tenga una vañloración por debajo del valor máximo o que el arbol esté vacio
	 * COMP:   O(todo)
	 */

	bool masValorado(Restaurante* &R, float valmax, const string &smax);

    /* DESC:   Escribe en el flujo informacion relativa a los restaurantes
     * PRE:    La instancia debe estar creada
     * POST:   En el flujo se escribe información de todos los visitantes y sus comensales actuales
     * PARAM:  E/S : flujo_salida -> Flujo donde se escribe la información -> ofstream
     * RET:    -
     * COMP:   O(n²)
     */

	void salida(ofstream &flujo_escritura);

};

#endif /* RESTAURANTES_H_ */
