//=================================================================================================
// Nombre      : VisitantesNA.cpp
// Proyecto    : GASTROEDI_2015
// Autor       : Pablo Macias Munoz
// Descripción : Implementanción de la clase VisitantesNA
//=================================================================================================

#include "VisitantesNA.h"

//Métodos Privados

void VisitantesNA::mostrarCola(Cola<Visitante *> *&c) {

	Visitante *Vaux; //Visitante auxiliar

	Cola<Visitante *> *caux = new Cola<Visitante *>(); //Cola auxiliar

	int i = 1; //Contador auxiliar

	while (!c->vacia()) { //Mientras la cola principal no esta vacia...

		c->primero(Vaux); //El primer elemento se guarda en Vaux

		cout << "\n_______________Visitante " << i << "_________________" << endl; //Muestra mensaje

		Vaux->mostrar(); //Muestra al Visitante

		caux->encolar(Vaux); //Encola el visitante en la cola auxiliar

		c->desencolar(); //Desencola el primer elemento de la cola principal

		i++; //Aumenta el indice (orden de visitante)

	}

	c = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

	delete caux;

}

void VisitantesNA::salidaCola(Cola<Visitante *> *&c, ofstream &flujo_salida){

	Visitante *Vaux; //Visitante auxiliar

	Cola<Visitante *> *caux = new Cola<Visitante *>(); //Cola auxiliar

	while (!c->vacia()) { //Mientras la cola principal no esta vacia...

		c->primero(Vaux); //El primer elemento se guarda en Vaux

		Vaux->salidaNA(flujo_salida); //Muestra al Visitante (salida en flujo)

		caux->encolar(Vaux); //Encola el visitante en la cola auxiliar

		c->desencolar(); //Desencola el primer elemento de la cola principal

	}

	c = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

	delete caux;

}

//Constructores y destructores

VisitantesNA::VisitantesNA() {

	visitantes_NA = new Cola<Visitante *>();

}

VisitantesNA::~VisitantesNA() {

	Visitante *Vaux;

	while (!visitantes_NA->vacia()) {

		visitantes_NA->primero(Vaux);

		visitantes_NA->desencolar();

		delete Vaux;

	}

	delete visitantes_NA;

	visitantes_NA = NULL;
}

//Métodos públicos

void VisitantesNA::mostrar() {

	cout << "\n========================================="
			"\n==       VISITANTES NO ATENDIDOS       =="
			"\n=========================================" << endl;

	mostrarCola(visitantes_NA);

}

void VisitantesNA::insertarVisitante(Visitante *V) {

	visitantes_NA->encolar(V);

}

void VisitantesNA::borrarVisitante(const string &nombre, const string &apellidos) {

	Visitante *V = new Visitante(nombre, apellidos); //Visitante auxiliar con nombre y apellidos

	Visitante *Vaux; //Visitante temportal

	Cola<Visitante *> *caux = new Cola<Visitante *>(); //Cola auxiliar

	while (!visitantes_NA->vacia()) { //Mientras la cola no est� vacia

		visitantes_NA->primero(Vaux); //El primer elemento se guarda en Vaux

		if (!Vaux->operator ==(*V)) //Si Vaux(visitante) no es igual al visitante donde apunta V

			caux->encolar(Vaux); //Encola paux en la cola auxiliar

		visitantes_NA->desencolar(); //Desencola el primer elemento de la cola principal

	}

	delete V;

	Vaux = NULL;

	delete visitantes_NA; //Elimina la cola principal

	visitantes_NA = caux; //c apunta donde apunta caux

	caux = NULL; //caux apunta a NULL

}

bool VisitantesNA::buscarVisitante(const string &nombre, const string &apellidos, Visitante * &V) {

	Visitante *Vaux = new Visitante(nombre, apellidos); //Visitante auxiliar con nombre y apellidos

	Cola<Visitante *> *caux = new Cola<Visitante *>(); //Cola auxiliar

	bool encontrado = false; //Bandera auxiliar

	while (!visitantes_NA->vacia()) { //Mientras la cola no est� vacia

		visitantes_NA->primero(V); //El primer elemento se guarda en V

		if (Vaux->operator ==(*V) && !encontrado) { //Si Vaux es igual que el Visitante de la cola y no esta encontrado...

			delete Vaux; //Elimina Vaux

			Vaux = V; //Vaux apunta donde apunta V

			encontrado = true; //Bandera a true -> Preferencia encontrada

		}

		caux->encolar(V); //Encola V en la cola auxiliar

		visitantes_NA->desencolar(); //Desencola el primer elemento de la cola principal

	}

	delete visitantes_NA; //Elimina la cola principal

	visitantes_NA = caux; //c apunta donde apunta caux

	if (encontrado) //Si se ha encontrado

		V = Vaux; //V apunta donde apunta Vaux

	else {

		V = NULL; //V apunta a NULL

		delete Vaux; //Elimina Vaux

	}

	Vaux = NULL; //Vaux apunta a NULL

	caux = NULL; //caux apunta a NULL

	return encontrado; //Devuelve encontrado(bandera)

}

void VisitantesNA::primerVisitante(Visitante * &V) {

	if (!visitantes_NA->vacia()) {

		visitantes_NA->primero(V);

		visitantes_NA->desencolar();
	}

	else

		V = NULL;

}

void VisitantesNA::salida(ofstream &flujo_salida){

	this->salidaCola(this->visitantes_NA, flujo_salida);

}
