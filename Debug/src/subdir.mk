################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/GastroEDI.cpp \
../src/Preferencias.cpp \
../src/Restaurante.cpp \
../src/Restaurantes.cpp \
../src/UI.cpp \
../src/Visitante.cpp \
../src/Visitantes.cpp \
../src/VisitantesNA.cpp 

OBJS += \
./src/GastroEDI.o \
./src/Preferencias.o \
./src/Restaurante.o \
./src/Restaurantes.o \
./src/UI.o \
./src/Visitante.o \
./src/Visitantes.o \
./src/VisitantesNA.o 

CPP_DEPS += \
./src/GastroEDI.d \
./src/Preferencias.d \
./src/Restaurante.d \
./src/Restaurantes.d \
./src/UI.d \
./src/Visitante.d \
./src/Visitantes.d \
./src/VisitantesNA.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


